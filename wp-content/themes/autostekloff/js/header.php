<!DOCTYPE html>
    <!--[if IE 6]>
    <html id="ie6" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if IE 7]>
    <html id="ie7" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if IE 8]>
    <html id="ie8" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <title><?php wp_title('|'); ?></title>
        <?php
        /* Always have wp_head() just before the closing </head>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to add elements to <head> such
         * as styles, scripts, and meta tags.
         */
        wp_head();
        ?>
        <script type="text/javascript" src="http://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit" async
                defer></script>

    </head>
<body>
    <?php global $autostekloff; ?>
    <header class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 nav">
                    <ul>
                        <li><a href=" http://autostekloff.ru/255-2/">Подобрать стекло</a></li>
                        <li><a href="http://autostekloff.ru/avtoservisy/">Автосервисы</a></li>
                        <li><a href="http://autostekloff.ru/kompaniya/">Компания</a></li>
                        <li><a href="http://autostekloff.ru/partneram/">Партнерам</a></li>
                        <li><a href="http://autostekloff.ru/blog/">Блог</a></li>
                        <li><a href="http://autostekloff.ru/kontakty/">Контакты</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7 col-sm-8">
                    <div class="row">
                        <div class="col-sm-4 logo">
                            <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/logo.png" alt=""></a>
                        </div>
                        <div class="col-sm-4 addr">
                            <span>Ваш город:</span><a href="#" class="city">Москва</a>
                            <div class="float">
                                <ul>
                                    <li><a href="#">Москва</a></li>
                                    <li><a href="#">Краснодар</a></li>
                                    <li><a href="#">Тверь</a></li>
                                    <li><a href="#">Самара</a></li>
                                    <li><a href="#">Пенза</a></li>
                                    <li><a href="#">Красноуфимск</a></li>
                                    <li><a href="#">Выборг</a></li>
                                    <li><a href="#">Владивосток</a></li>
                                    <li><a href="#">Керчь</a></li>
                                    <li><a href="#">Пермь</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-4 phone">
                            <p>+7 (499) 686-08-45</p>
                            <a href="#">Задать вопрос</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-4">
                    <div class="row">
                        <div class="col-sm-6 col-md-3 login">
                            <a href="#" data-toggle="modal" data-target="#enter">Личный кабинет</a>
                        </div>
                        <div class="col-sm-6 col-md-9 exts">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-4 ext">
                                            <div class="row"><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_nav5.png"><span>0</span></a></div>
                                        </div>
                                        <div class="col-md-4 ext">
                                            <div class="row"><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_nav4.png" alt=""><span>0</span></a></div>
                                        </div>
                                        <div class="col-md-4 ext">
                                            <div class="row"><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_nav2.png" alt=""><span>1</span></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-12 ext cart">
                                    <div class="row"><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_nav3.png" alt=""><span>3</span></a>
                                        <p><a href="#">Корзина<span>12 750 Р</span></a></p></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>