(function($){
    $(document).on('ready', function () {
        $('body').on('change', '#choose_glass', function (e) {
            e.preventDefault();
            var data = $(this).val();
            $.ajax({
                url: php_vars.url,
                type: 'get',
                data: {
                    action: 'step_two_get_json',
                    eurocode: data
                },
                success: function (data) {
                    array_eurocode = [];
                    for(var i = 0; i < data.data.length; i++) {
                        array_eurocode.push(data.data[i].eurocode);
                    }
                    render(data, true, array_eurocode);

                }
            })
        });


    });
    function render(d, flag, code) {
        var masDisplayBlock  = [];
        var  massGlass = [];
        var masDopInfo = [];
        var checkblock = $('.checkblock');
        checkblock.html('<table></table>');
        var resultblock = $('.result');
        resultblock.html(' ');
        var radioblock = $('.radioblock');
        radioblock.html(' ');
        // if (autoStatus) {
        //     globalCount = +(localStorage.getItem('globalCountStart')) - 1;
        // }
        //
        // if (!autoStatus) {
        //     localStorage.setItem('filter', '[]');
        // }
        // console.log(localStorage.getItem('filter'));
        // $(table_eurocode).find('tbody').find('tr').remove();
        // $(table_eurocode_searchCar).find('tbody').find('tr').remove();
        // $('.setProperties:not([data-id="mainControll"]) > label').remove();
        var table = $(document.body).append('<table></table>');
        if (!flag) {
            var tbody = $(table);
        } else {
            var tbody = $(table);
        }
        // var myObj = JSON.parse(d);

        // console.log(myObj);
        var count = 0;
        var sideWindowWord = ['F', 'H', 'L', 'M', 'R', 'T'];
        var rearWindowWord = ['B', 'E'];
        var windShieldWord = ['A', 'C', 'D'];
        var prioritetMas = [];
        var accessories = '';

        var segment = [{"id": "1", "name": "3M", "segment": "budget"}, {
            "id": "2",
            "name": "ABC AUTOGLASS - PPG",
            "segment": "business"
        }, {"id": "3", "name": "AGC", "segment": "premium"}, {
            "id": "4",
            "name": "AIRLOC",
            "segment": "budget"
        }, {"id": "5", "name": "AUTO CURTAIN", "segment": "budget"}, {
            "id": "6",
            "name": "BENSON",
            "segment": "econom"
        }, {"id": "7", "name": "BSG", "segment": "budget"}, {
            "id": "8",
            "name": "CARALL",
            "segment": "budget"
        }, {"id": "9", "name": "CDC", "segment": "budget"}, {
            "id": "10",
            "name": "COX",
            "segment": "budget"
        }, {"id": "11", "name": "DAEWOO", "segment": "budget"}, {
            "id": "12",
            "name": "DORA GLASS",
            "segment": "budget"
        }, {"id": "13", "name": "DOW", "segment": "budget"}, {
            "id": "14",
            "name": "EQUALIZER",
            "segment": "budget"
        }, {"id": "15", "name": "EUROGLASS", "segment": "budget"}, {
            "id": "16",
            "name": "FLEXLINE",
            "segment": "budget"
        }, {"id": "17", "name": "FYG", "segment": "econom"}, {
            "id": "18",
            "name": "GUARDIAN",
            "segment": "business"
        }, {"id": "19", "name": "HANGLASS", "segment": "budget"}, {
            "id": "20",
            "name": "HENKEL",
            "segment": "budget"
        }, {"id": "21", "name": "HYUNDAI", "segment": "business"}, {
            "id": "22",
            "name": "INFINITI",
            "segment": "business"
        }, {"id": "23", "name": "KMK", "segment": "budget"}, {
            "id": "24",
            "name": "KOREA AUTOGLASS",
            "segment": "budget"
        }, {"id": "25", "name": "KSB", "segment": "budget"}, {
            "id": "26",
            "name": "LEXUS",
            "segment": "business"
        }, {"id": "27", "name": "LUCKYGLASS", "segment": "budget"}, {
            "id": "28",
            "name": "MERCEDES",
            "segment": "business"
        }, {"id": "29", "name": "MITSUBISHI", "segment": "business"}, {
            "id": "30",
            "name": "NF",
            "segment": "budget"
        }, {"id": "31", "name": "NISSAN", "segment": "business"}, {
            "id": "32",
            "name": "NORD GLASS",
            "segment": "business"
        }, {"id": "33", "name": "NORDGLASS", "segment": "business"}, {
            "id": "34",
            "name": "OEM",
            "segment": "business"
        }, {"id": "35", "name": "OLIMP", "segment": "budget"}, {
            "id": "36",
            "name": "OLIMPIA",
            "segment": "budget"
        }, {"id": "37", "name": "PGW", "segment": "business"}, {
            "id": "38",
            "name": "PILK",
            "segment": "premium"
        }, {"id": "39", "name": "PILKINGTON", "segment": "premium"}, {
            "id": "40",
            "name": "PMA",
            "segment": "budget"
        }, {"id": "41", "name": "PMA TOOLS", "segment": "budget"}, {
            "id": "42",
            "name": "PPG",
            "segment": "business"
        }, {"id": "43", "name": "PROGLASS", "segment": "budget"}, {
            "id": "44",
            "name": "SAINT-GOBAIN SOLAR GARD",
            "segment": "budget"
        }, {"id": "45", "name": "SAT", "segment": "budget"}, {
            "id": "46",
            "name": "SC",
            "segment": "budget"
        }, {"id": "47", "name": "SEJIN GLASS", "segment": "budget"}, {
            "id": "48",
            "name": "SEKURIT",
            "segment": "premium"
        }, {"id": "49", "name": "SHATTERPRUFE", "segment": "business"}, {
            "id": "50",
            "name": "SICURSIV",
            "segment": "budget"
        }, {"id": "51", "name": "SPEKTR", "segment": "budget"}, {
            "id": "52",
            "name": "SPLINTEX",
            "segment": "budget"
        }, {"id": "53", "name": "SSANGYONG", "segment": "business"}, {
            "id": "54",
            "name": "STARGLASS",
            "segment": "budget"
        }, {"id": "55", "name": "STEKLOLUX", "segment": "budget"}, {
            "id": "56",
            "name": "TAMGLASS",
            "segment": "budget"
        }, {"id": "57", "name": "TBM", "segment": "budget"}, {
            "id": "58",
            "name": "TOYOTA",
            "segment": "business"
        }, {"id": "59", "name": "WEISS", "segment": "budget"}, {
            "id": "60",
            "name": "XST",
            "segment": "budget"
        }, {"id": "61", "name": "XYG", "segment": "econom"}, {
            "id": "62",
            "name": "КМК",
            "segment": "budget"
        }, {"id": "63", "name": "ОАО \"Эй Джи Си БСЗ\"", "segment": "budget"}, {
            "id": "64",
            "name": "СТЕКЛА ДЛЯ АВТОБУСОВ",
            "segment": "budget"
        }];

        var myObj =  d.data;
var countMyObj = myObj.length;
        outer:for (var i = 0; i < countMyObj; i++) {
                var eurocode = myObj[i].eurocode;
            var content_f;
            $.get('http://test.avtosteklov.ru/shop/search/description_by_ec/?ec='+eurocode, function(response) {
                glass = response.glass;
                var mass = [];
                mass[i] = {i : glass};
                massGlass.push(glass);
                content_f = response.content_f;
            });
            // console.log(globalCount);
            // globalCount += 1;
            // if (i == 0 && !autoStatus) localStorage.setItem('globalCountStart', globalCount)
            //  if (i+1 == myObj.length && !autoStatus) localStorage.setItem('globalCountEnd',globalCount)

            // INSPECTION //
            //    if (eurocode[5] == 'K' || eurocode[5] == 'S') continue;
            var getType = eurocode[4];
            var eurocode_color = eurocode.substring(5, 7);

            if ($.inArray(getType, sideWindowWord) != -1) {
                var eurocode_type = eurocode.substring(7, 9);
                var eurocode_thirdParam = eurocode.substring(9, 11);
                var eurocode_thirdParamName = 'data-glassPosition';
                var dop = {
                    'A': 'Антенна',
                    'D': 'Стеклопакет',
                    'E': 'Электрические опции',
                    'F': 'Рамка с подв. и неподв. стеклом',
                    'G': 'GPS',
                    'H': 'С обогревом',
                    'K': 'Триплекс',
                    'O': 'Открывающееся',
                    'Q': 'Выдвигающаяся Антенна',
                    'V': 'VIN',
                    'X': 'Провод сигнализации'
                };


                for (var k = 11; k < eurocode.length; k++) {
                    if (isNaN(Number(eurocode[k])) == false) break
                    else if (eurocode[k] == '-') break;
                    for (var key in dop) {
                        if (eurocode[k] == key) {
                            masDopInfo.push(dop[key]);
                        }
                    }
                }
            } else if ($.inArray(getType, rearWindowWord) != -1) {
                var eurocode_type = eurocode.substring(7, 8);
                var eurocode_thirdParam = '';
                var eurocode_thirdParamName = '';
                var dop = {
                    'A': 'Антенна',
                    'B': 'Стоп-сигнал',
                    'C': 'Центральная часть',
                    'D': 'Стеклопакет',
                    'F': 'Плоское стекло',
                    'K': 'Триплекс',
                    'M': 'Автосистема аварийных оповещений',
                    'G': 'GPS',
                    'H': 'С обогревом',
                    'P': 'Антенна для мобильного телефона',
                    'Q': 'Антенна для открывания дверей',
                    'U': 'Без обогрева',
                    'X': 'Провод сигнализации',
                    'J': 'ТВ Антенна',
                    'O': 'Открывающееся',
                    'V': 'VIN окно'
                };
                var masDopInfo = [];
                for (var k = 8; k < eurocode.length; k++) {
                    if (isNaN(Number(eurocode[k])) == false) {
                        break;
                    } else if (eurocode[k] == '-') break;
                    for (var key in dop) {
                        if (eurocode[k] == key) {
                            masDopInfo.push(dop[key]);
                        }
                    }
                }
            } else if ($.inArray(getType, windShieldWord) != -1) {
                var vehicleData = {
                    'BABL': 'Голубое + Шумоизоляция/Голубая полоса',
                    'BAGN': 'Голубое + Шумоизоляция/Зеленая полоса',
                    'BBBL': 'Голубое Теплопоглощающее/Голубая полоса',
                    'BBGN': 'Голубое Теплопоглощающее/Зеленая полоса',
                    'BBYN': 'Голубое Теплопоглощающее/Серая полоса',
                    'BLBL': 'Голубое /Голубая полоса',
                    'BLGN': 'Голубое /Зеленая полоса',
                    'BSBL': 'Голубое Solar Control/Голубая полоса',
                    'BSGN': 'Голубое Solar Control/Зеленая полоса',
                    'BSGY': 'Голубое Solar Control/Серая полоса',
                    'BZBZ': 'Бронзовое/Бронзовая полоса',
                    'BZGN': 'Бронзовое/Зеленая полоса',
                    'BZBL': 'Бронзовое/Голубая полоса',
                    'BZGY': "Бронзовое/Серая полоса",
                    'CBBL': 'Прозрачное Поглощающее IR и UV/Голубая полоса',
                    'CBBZ': 'Прозрачное Поглощающее IR и UV/Бронзовая полоса',
                    'CBGN': 'Прозрачное Поглощающее IR и UV/Зеленая полоса',
                    'CBGY': "Прозрачное Поглощающее IR и UV/Серая полоса",
                    'CCBL': 'Прозрачное/Теплоотражающее/Голубая полоса',
                    'CCBZ': 'Прозрачное/Теплоотражающее/Бронзовая полоса',
                    'CCGN': 'Прозрачное/Теплоотражающее/Зеленая полоса',
                    'CCGY': 'Прозрачное/Теплоотражающее/Серая полоса',
                    'CDBL': 'Прозрачное/Теплоотражающее+Шумоизоляция/Голубая полоса',
                    'CDBZ': 'Прозрачное/Теплоотражающее+Шумоизоляция/Бронзовая полоса',
                    'CDGN': 'Прозрачное/Теплоотражающее+Шумоизоляция/Зеленая полоса',
                    'CDGY': 'Прозрачное/Теплоотражающее+Шумоизоляция/Серая полоса',
                    'CLBL': 'Прозрачное/Голубая полоса',
                    'CLBZ': 'Прозрачное/Бронзовая полоса',
                    'CLGN': 'Прозрачное/Зеленая полоса',
                    'CLGY': 'Прозрачное/Серая полоса',
                    'GBBL': 'Зеленое Поглощающее IR и UV/Голубая полоса',
                    'GBBZ': 'Зеленое Поглощающее IR и UV/Бронзовая полоса',
                    'GBGN': 'Зеленое Поглощающее IR и UV/Зеленая полоса',
                    'GBGY': 'Зеленое Поглощающее IR и UV/Серая полоса',
                    'GABL': 'Зеленое + Шумоизоляция/Голубая полоса',
                    'GABZ': 'Зеленое + Шумоизоляция/Бронзовая полоса',
                    'GAGN': 'Зеленое + Шумоизоляция/Зеленая полоса',
                    'GAGY': 'Зеленое + Шумоизоляция/Серая полоса',
                    'GNBL': 'Зеленое/Голубая полоса',
                    'GNBZ': 'Зеленое/Бронзовая полоса',
                    'GNGN': 'Зеленое/Зеленая полоса',
                    'GNGY': 'Зеленое/Серая полоса',
                    'GSBL': 'Зеленое Solar Control/Голубая полоса',
                    'GSBZ': 'Зеленое Solar Control/Бронзовая полоса',
                    'GSGN': 'Зеленое Solar Control/Зеленая полоса',
                    'GSGY': 'Зеленое Solar Control/Серая полоса',
                    'GSYD': 'Зеленое Solar Control/Темно-Серая полоса',
                    'GYBL': 'Серое/Голубая полоса',
                    'GYGY': 'Серое/Серая полоса',
                    'LGBL': 'Светло Зеленое/Голубая полоса ',
                    'LGLG': 'Светло Зеленое/Светло Зеленая полоса ',
                    'YABL': 'Серое + Шумоизоляция/Голубая полоса',
                    'YAGY': 'Серое + Шумоизоляция/Серая полоса',
                    'YSBL': 'Серое Solar Control/Голубая полоса',
                    'YSGY': 'Серое Solar Control/Серая полоса'
                };

                if (eurocode.length < 5) {
                    continue;
                }
                var eurocode_type = eurocode.substring(5, 9);
                var eurocode_thirdParam = '';
                var eurocode_thirdParamName = '';
                var dop = {
                    'A': 'Антенна',
                    'B': 'Правый руль',
                    'C': 'Место для камеры ночьного видения/или предупреждения переулка Функциональные возможности ACC',
                    'G': 'GPS',
                    'H': 'С обогревом',
                    'J': 'ТВ Антенна',
                    'M': 'Датчик (Света и/или Дождя)',
                    'O': 'Датчик тумана',
                    'V': 'VIN окно',
                    'U': 'Дисплей для характеристик'
                };
                var masDopInfo = [];
                var polosaText = '';
                var polosa = false;

                var accesValue = '';

                for (var keyObj in vehicleData) {
                    if (keyObj == eurocode_type) {
                        polosa = true;
                        masDopInfo.push(vehicleData[keyObj]);
                    }
                }

                if (polosa) {
                    var k = 9;
                } else {
                    var k = 7;
                }
                var accesoriesWord = eurocode.substring(k, eurocode.length);
                //перебрать accesoriesWord и закинуть в массив
                for (var w = 0; w < accesoriesWord.length; w++) {
                    if (accesoriesWord[w] == '-') break;
                    if (isNaN(Number(accesoriesWord[w])) == false) break
                    accesValue += accesoriesWord[w];
                }

                for (; k < eurocode.length; k++) {
                    if (isNaN(Number(eurocode[k])) == false) break
                    else if (eurocode[k] == '-') break;
                    for (var key in dop) {
                        if (eurocode[k] == key) {
                            masDopInfo.push(dop[key]);
                        }
                    }
                }

            }

           // var glass;





            var textPopup = '';
            var prioritet = function (firm) {

                for (var i = 0; i < segment.length; i++) {
                    if (firm == segment[i]['name']) {
                        prioritetMas.push(segment[i]['segment']);
                        return segment[i]['segment']
                    }
                }

            }(myObj[i].manufacturer_name);

            var checkBoxStr = '<form><label for="offer{' + count + '}{1}"><input   data-positionId="' + myObj[i].price_position_id + '" type="checkbox" id="offer{' + count + '}{1}" class="offer_checkbox" /><p></p></label for="offer{' + count + '}{2}"><label><input data-positionId="' + myObj[i].price_position_id + '" type="checkbox" id="offer{' + count + '}{2}" class="offer_checkbox" /><p></p></label><label for="offer{' + count + '}{3}"><input data-positionId="' + myObj[i].price_position_id + '" type="checkbox" id="offer{' + count + '}{3}" class="offer_checkbox" /><p></p></label></form>';

// // output products
//
            $('.checkblock table').append('<tr class="role" data-accesories="' + accesValue + '" data-typeOf="' + getType + '" data-price-wi="' + myObj[i].sum_cost_wi + '" data-price="' + myObj[i].sum_cost + '" data-color="' + eurocode_color + '" data-type="' + eurocode_type + '" data-manufacturer="' + myObj[i].manufacturer_name + '" ' + eurocode_thirdParamName + '=' + '"' + eurocode_thirdParam + '" data-prioritet="' + prioritet + '"></tr>');
//
//
//
            $(masDopInfo).each(function (index, el) {
                // $(tbody).find('tr:last').attr('dopInfo_' + index, el);
                textPopup += el + '</br>';
            });
            textPopup += myObj[i].comment;
            $('.res_search').find('h3').text('мы нашли для вас '+ (i +1) +' стекол(ла)');
            var itemObj = myObj[i];
           function firstRenderItem(itemObj) {
                $('.result').append('<div class="item" data-segment="' + itemObj.manufacturer_segment + '" id="'+ i +'"><div class="clickable"><div class="col-md-2 col-sm-3 col-xs-12 img"><a href="#" class="show"></a><img src="<?php bloginfo(\'template_directory\')?>/img/glass1.png" alt=""></div> <div class="col-md-5 col-sm-5 col-xs-12 title"><strong>' + itemObj.name + '</strong>' + ' <p>' + content_f + '</p>' + '</div><div class="col-md-3 col-sm-4  col-xs-6 rating"><p>Рейтинг <b>4.50</b><br/><span>325 оценок</span></p><p class="label">Нет в наличии</p> </div><div class="col-md-2 col-sm-12 col-xs-6 "><div class="col-md-12 col-sm-6"> <p class="price old">' + itemObj.sum_crossed + '</p><p class="price">' + itemObj.sum_cost + '</p></div><div class="col-md-12 col-sm-6"><button class="btn-primary">Выбрать</button><div class="text-center"><a href="#" class="btn-compare"></a><a href="#" class="btn-star"></a></div></div> </div></div><div class="col-sm-12 body"><div class="row"><div class="col-md-5 col-md-offset-2 tabs"><ul class="nav nav-tabs" id="blogPages"><li class="active"><a data-toggle="tab" href="#tb11'+ i +'">Характеристики</a></li><li><a data-toggle="tab" href="#tb12'+ i +'">Датчики</a></li></ul><div class="tab-content"><div class="tab-pane fade in active" id="tb11'+ i +'"><table><tr><td>Размеры стекла</td><td>0x0</td></tr><div class="tab-pane fade" id="tb12'+ i +'"><table><tr><td>Размеры стекла</td><td>1409ч673</td></tr></table></div></div></div><div class="col-md-3 text"><strong>'+ itemObj.manufacturer_name + '</strong><p>'+ itemObj.manufacturer_description+'</p> <button class="btn-primary">О производителе <i></i></button></div></div></div></div>');
            }
            firstRenderItem(itemObj);


            // getFilter(myObj[i].eurocode, myObj[i], type);
        }
        //console.log(masDopInfo);
//console.log(prioritetMas);

        !function getMinMax() {

            var alldivprice = $('.result');

            var mas = [];
            masMinMax = [];
            $(alldivprice).each(function (i, el) {

                var price = +($(el).find('.price').text());
                mas.push(price);

            });
            //Находим минимальное и максимальное значение в массиве
            var min = Math.min.apply(Math, mas);
            var max = Math.max.apply(Math, mas);
            masMinMax.push(min, max);
        }();

//         function changeOffer() {
//             console.log('сработало');
//             var parent = $(this);
//             console.log(parent);
//
//             $(parent).siblings().find('input').attr('checked', false);
//             var siblingsTd = $(this).parents('tr').siblings().find('td:first-child');
//             var labels = $(siblingsTd).find('label');
//             $(labels).each(function (i, el) {
//                 var ind = $(el).index();
//                 if (ind == index) $(el).find('input').attr('checked', false);
//             })
//             var checkedOffer = $('.offer_checkbox:checked');
//             var parentTable = $(this).parents('table');
//             var link;
//
//             if ($(parentTable).attr('data-id') == 'table_eurocode_searchCar') {
//                 checkMainCheckBox($('#mainControll'))
//             }
//             else if ($(parentTable).attr('data-id') == 'table_eurocode') {
//                 checkMainCheckBox($('#mainControllEurocode'))
//             }
//             $('#ic').attr('href', link);
//             //Устанавливаем в сессию знаение ссылки для googleMap
//             sessionStorage.setItem('mapLink', link);
//             setLocalSt();
//             /**
//              *
//              * @param elem
//              */
//             function checkMainCheckBox(elem) {
//
//                 if ($(elem).prop('checked')) var flag = 'true'
//                 else var flag = 'false';
//                 link = '/install/?'
//                 $(checkedOffer).each(function (i, el) {
//                     var index = $(el).parent().index();
//                     if (i == 0) link += 'price_position[' + index + ']=' + $(el).attr('data-positionId');
//                     else link += '&price_position[' + index + ']=' + $(el).attr('data-positionId');
//                 })
//                 link += '&cityId=' + $('.city').attr('data-cityId');
//                 link += '&minPrice=' + masMinMax[0] + '&maxPrice=' + masMinMax[1];
//                 link += '&flag=' + flag;
//                 sessionStorage.setItem('mapLink', link);
//
//             };
// //
//             function setLocalSt() {
//                 localOfferMas = [];
//                 $('.offer_checkbox:checked').each(function (i, el) {
//                     var id = $(el).attr('id');
//                     localOfferMas.push(id);
//                 })
//                 localStorage.setItem('offerItems', JSON.stringify(localOfferMas))
//                 console.log(localOfferMas);
//             }
//         }

        /* КОНЕЦ ПЕРЕБОРА */
        if (flag) {

            // var top = $(table_eurocode_searchCar).offset().top - $('.top').height();
            getFilter(code, myObj, 'searchCar');//вызваем функцию филтрации
        }
        else {
            // var top = $(table_eurocode).offset().top - $('.top').height();
            getFilter(code, myObj, 'searchEurocode');//вызваем функцию филтрации
        }
        ;
        //
        //
        // $('html, body').animate({scrollTop: top}, 1000);

        /* ФЛЬТР  */
        /**
         *
         * @param eurocode
         * @param obj
         * @param type
         */
        function getFilter(eurocode, obj, type) {
            if (eurocode.length < 5) return;
            var filterCount = 0;
            var textVehicleBodyType;


            var colorsTableSideWindow = function () {
                var colorsMas = [];
                var vehicleBodyMas = [];
                var manufacturerMas = [];
                var resultMas = [];
                var glassPositionMas = [];
                var priceMas = {};
                var typeMas = [];
                var accesoriesWordMas = [];
                for (var i = 0; i < obj.length; i++) {
                    var eurocode = obj[i].eurocode.toUpperCase();
                    for (var n = 0; n < 4; n++) {
                        var toNum = Number(eurocode[n]);
                        if (isNaN(toNum)) break;

                    }

                    //INSPECTION
                    if (eurocode[5] == 'K' || eurocode[5] == 'S') continue;
                    var getType = eurocode[4];
                    typeMas.push(getType);
                    if ($.inArray(getType, sideWindowWord) != -1) {
                        var vehicleBody = eurocode.substring(7, 9);
                    }
                    else if ($.inArray(getType, rearWindowWord) != -1) var vehicleBody = eurocode.substring(7, 8)
                    else if ($.inArray(getType, windShieldWord) != -1) {

                        var vehicleData = {
                            'BABL': 'Голубое + Шумоизоляция/Голубая полоса',
                            'BAGN': 'Голубое + Шумоизоляция/Зеленая полоса',
                            'BBBL': 'Голубое Теплопоглощающее/Голубая полоса',
                            'BBGN': 'Голубое Теплопоглощающее/Зеленая полоса',
                            'BBYN': 'Голубое Теплопоглощающее/Серая полоса',
                            'BLBL': 'Голубое /Голубая полоса',
                            'BLGN': 'Голубое /Зеленая полоса',
                            'BSBL': 'Голубое Solar Control/Голубая полоса',
                            'BSGN': 'Голубое Solar Control/Зеленая полоса',
                            'BSGY': 'Голубое Solar Control/Серая полоса',
                            'BZBZ': 'Бронзовое/Бронзовая полоса',
                            'BZGN': 'Бронзовое/Зеленая полоса',
                            'BZBL': 'Бронзовое/Голубая полоса',
                            'BZGY': "Бронзовое/Серая полоса",
                            'CBBL': 'Прозрачное Поглощающее IR и UV/Голубая полоса',
                            'CBBZ': 'Прозрачное Поглощающее IR и UV/Бронзовая полоса',
                            'CBGN': 'Прозрачное Поглощающее IR и UV/Зеленая полоса',
                            'CBGY': "Прозрачное Поглощающее IR и UV/Серая полоса",
                            'CCBL': 'Прозрачное/Теплоотражающее/Голубая полоса',
                            'CCBZ': 'Прозрачное/Теплоотражающее/Бронзовая полоса',
                            'CCGN': 'Прозрачное/Теплоотражающее/Зеленая полоса',
                            'CCGY': 'Прозрачное/Теплоотражающее/Серая полоса',
                            'CDBL': 'Прозрачное/Теплоотражающее+Шумоизоляция/Голубая полоса',
                            'CDBZ': 'Прозрачное/Теплоотражающее+Шумоизоляция/Бронзовая полоса',
                            'CDGN': 'Прозрачное/Теплоотражающее+Шумоизоляция/Зеленая полоса',
                            'CDGY': 'Прозрачное/Теплоотражающее+Шумоизоляция/Серая полоса',
                            'CLBL': 'Прозрачное/Голубая полоса',
                            'CLBZ': 'Прозрачное/Бронзовая полоса',
                            'CLGN': 'Прозрачное/Зеленая полоса',
                            'CLGY': 'Прозрачное/Серая полоса',
                            'GBBL': 'Зеленое Поглощающее IR и UV/Голубая полоса',
                            'GBBZ': 'Зеленое Поглощающее IR и UV/Бронзовая полоса',
                            'GBGN': 'Зеленое Поглощающее IR и UV/Зеленая полоса',
                            'GBGY': 'Зеленое Поглощающее IR и UV/Серая полоса',
                            'GABL': 'Зеленое + Шумоизоляция/Голубая полоса',
                            'GABZ': 'Зеленое + Шумоизоляция/Бронзовая полоса',
                            'GAGN': 'Зеленое + Шумоизоляция/Зеленая полоса',
                            'GAGY': 'Зеленое + Шумоизоляция/Серая полоса',
                            'GNBL': 'Зеленое/Голубая полоса',
                            'GNBZ': 'Зеленое/Бронзовая полоса',
                            'GNGN': 'Зеленое/Зеленая полоса',
                            'GNGY': 'Зеленое/Серая полоса',
                            'GSBL': 'Зеленое Solar Control/Голубая полоса',
                            'GSBZ': 'Зеленое Solar Control/Бронзовая полоса',
                            'GSGN': 'Зеленое Solar Control/Зеленая полоса',
                            'GSGY': 'Зеленое Solar Control/Серая полоса',
                            'GSYD': 'Зеленое Solar Control/Темно-Серая полоса',
                            'GYBL': 'Серое/Голубая полоса',
                            'GYGY': 'Серое/Серая полоса',
                            'LGBL': 'Светло Зеленое/Голубая полоса ',
                            'LGLG': 'Светло Зеленое/Светло Зеленая полоса ',
                            'YABL': 'Серое + Шумоизоляция/Голубая полоса',
                            'YAGY': 'Серое + Шумоизоляция/Серая полоса',
                            'YSBL': 'Серое Solar Control/Голубая полоса',
                            'YSGY': 'Серое Solar Control/Серая полоса'
                        };

                        if (eurocode.length < 9) continue;
                        var eurocode_type = eurocode.substring(5, 9);
                        var vehicleBody = eurocode.substring(5, 9);
                        //проверка на полосу
                        var polosa = false;
                        var k;
                        for (var keyObj in vehicleData) {
                            if (keyObj == eurocode_type) {
                                polosa = true;
                            }
                        }
                        ;
                        polosa ? k = 9 : k = 7;
                        var accesoriesWord = eurocode.substring(k, eurocode.length);
                        //заполняем массив с аксессуарами
                        for (var w = 0; w < accesoriesWord.length; w++) {
                            if (accesoriesWord[w] == '-') break;
                            if (isNaN(Number(accesoriesWord[w])) == false) break
                            accesoriesWordMas.push(accesoriesWord[w]);
                        }
                        ;
                        if (!polosa) vehicleBody = false;
                        if (vehicleData[vehicleBody] == undefined && polosa == true) continue;

                    }
                    if ($.inArray(getType, sideWindowWord) != -1) var glassPosition = eurocode.substring(9, 11);
                    var manufacturer = obj[i].manufacturer_name;
                    var color = eurocode.substring(5, 7);

                    if (vehicleBody) vehicleBodyMas.push(vehicleBody);
                    colorsMas.push(color);
                    manufacturerMas.push(manufacturer);
                    //prioritetMas.push()
                    priceMas[obj[i].manufacturer_name + '_' + i] = obj[i].sum_cost;
                    priceMas['marja_' + i] = obj[i].cost_margin;


                    if ($.inArray(getType, sideWindowWord) != -1) glassPositionMas.push(glassPosition);
                }
                ;

                if ($.inArray(getType, sideWindowWord) != -1) removeRepeat(glassPositionMas);
                removeRepeat(colorsMas);
                removeRepeat(vehicleBodyMas);
                removeRepeat(manufacturerMas);
                removeRepeat(prioritetMas);
                removeRepeat(typeMas);
                removeRepeat(accesoriesWordMas);

//выбираем минимальное значение и заносим в массив
                var minPrice = function () {
                    var mas = {};
                    for (var key in priceMas) {
                        var price = [];
                        var namePrice = [];
                        var manName;
                        var keySplit = key.split('_');
                        if (keySplit[0] == 'marja') continue;
                        for (var keygen in priceMas) {
                            var keygenSplit = keygen.split('_');
                            if (keySplit[0] == keygenSplit[0]) {
                                price.push(priceMas[keygen]);
                                namePrice.push(keygen);
                                delete priceMas[keygen];
                            }
                        }

                        var min = Math.min.apply(Math, price);

                        var index;
                        price.forEach(function (el, i) {

                            if (min == el) {
                                manName = +(namePrice[i].split('_')[1]);
                                index = i;
                            }

                        })
                        mas[keySplit[0]] = min;
                        mas[keySplit[0] + '_marja'] = priceMas['marja_' + manName];
                    }
                    return mas;
                }();

//убираем повторяющиеся значения

                function removeRepeat(mas) {
                    mas.forEach(function (el, i) {
                        for (var k = i + 1; k < mas.length; k++) {
                            if (el == mas[k]) {
                                mas.splice(k, 1);
                                k = i
                            }
                            ;
                        }
                        ;
                    })
                };
//возвращаем массв цветов которые есть в таблице
                resultMas.push(colorsMas, vehicleBodyMas, manufacturerMas, minPrice, prioritetMas, typeMas);

                if ($.inArray(getType, sideWindowWord) != -1) {
                    resultMas.push(glassPositionMas);
                }
                else if ($.inArray(getType, windShieldWord) != -1) {
                    resultMas.push(accesoriesWordMas);
                }

                return resultMas;

            };
            if (type == 'searchCar') {

                var paramFilter = $('.checkblock');
                var manufacturerFilter = $('.checkblock');
                var prioritetFilter = $('.checkblock');
                var parentElem = $('.formSearchCar');

            }
            else if (type == 'searchEurocode') {

                var paramFilter = $('#paramFilterEurocode');
                var manufacturerFilter = $('#manufacturerFilterEurocode');
                var prioritetFilter = $('#prioritetEurocode');
                var parentElem = $('.formEurocode');

            }
            var filterData = colorsTableSideWindow();
            filterData[5].forEach(function (el, i) {
                var getType = el;
                if (getType != undefined) {

                    if ($.inArray(getType, sideWindowWord) != -1) {
                        var colorData = {
                            'AB': 'Прозрачное противоударное (5 слоев)',
                            'AS': 'Прозначное противоударное (3 слоя)',
                            'BB': 'Голубое/Теплопоглощающее',
                            'BD': 'Темно Голубое',
                            'BL': 'Голубое',
                            'BP': 'Голубое - Privacy Glass',
                            'BS': 'Голубое-Privasy Glass',
                            'BS': 'Голубое-Solar Control',
                            'BZ': 'Бронзовое',
                            'CB': 'Прозрачное Теплопоглощающее',
                            'CC': 'Прозрачное/Теплоотражающее',
                            'CD': 'Прозрачное/Теплоотражающее+Шумоизоляция',
                            'CF': 'Прозрачное - Матовое',
                            'CL': 'Прозрачное',
                            'GA': 'Зеленое+Шумоизоляция',
                            'GB': 'Зеленое Теплопоглощающее',
                            'GD': 'Темно Зеленое',
                            'GN': 'Зеленое',
                            'GP': 'Зеленое - Privacy Glass',
                            'GQ': 'Зеленое - Privacy Glass Шумоизоляция',
                            'GS': 'Зеленое - Solar Control',
                            'GY': 'Серое',
                            'LG': 'Светло Зеленое (Japanese)',
                            'YC': 'Серое Теплоотражающее',
                            'YD': 'Темно Серое',
                            'YP': 'Серое - Privacy Glass',
                            'YS': 'Серое - Solar Control',
                            'ZD': 'Темно Бронзовое',
                            'ZP': 'Бронзовое - Privacy Glass',
                            'ZS': 'Бронзовое - Solar Control'
                        };
                        var vehicleData = {
                            'H5': 'Хетчбек -5dr',
                            'C2': 'Купе - 2dr',
                            'H3': 'Хетчбек - 3dr',
                            'S2': 'Седан - 2dr',
                            'S4': 'Седан - 4dr',
                            'S6': 'Лимузин - 6dr',
                            'C2': 'Купе - 2dr',
                            'C4': 'Купе - 4dr',
                            'E3': 'Комби - 3dr',
                            'E5': 'Комби - 5dr',
                            'M3': 'MPV - 3dr (*)',
                            'M4': 'MPV - 4dr (*)',
                            'M5': 'MPV - 5 dr (*)',
                            'R3': 'Ranger - 3dr',
                            'R5': 'Ranger - 5dr',
                            'T2': 'Кабриолет - 2dr',
                            'L2': 'Грузовик - 2dr',
                            'L4': 'Грузовик - 4dr',
                            'V2': 'Минивен - 2dr',
                            'V3': 'Минивен - 3dr',
                            'V4': 'Минивен - 4dr',
                            'V5': 'Минивен - 5dr',
                            'P2': 'Пикап - 2dr',
                            'P4': 'Пикап - 4dr'
                        };
                        var glassPositionData = {
                            'FD': 'Передняя дверь опускная',
                            'FQ': 'Передняя дверь неподвижная',
                            'FV': 'Передняя форточка',
                            'MD': 'Средняя дверь опускная',
                            'MQ': 'Средняя дверь неподвижная',
                            'PG': 'Стекло в салоне (перегородка)',
                            'RD': 'Задняя дверь опускная',
                            'RQ': 'Заняя дверь неподвижная',
                            'RV': 'Задняя форточка'
                        };
                        //выбираем элементы фильтра

                        textVehicleBodyType = 'Vehicle Body Type';

                    }

                    else if ($.inArray(getType, rearWindowWord) != -1) {

                        var colorData = {
                            'GN': 'Зелёное',
                            'AB': 'Прозрачное противоударное (5 слоев)',
                            'AS': 'Прозначное противоударное (3 слоя)',
                            'BA': 'Голубое+Шумоизоляция',
                            'BB': 'Голубое/Теплопоглощающее',
                            'BL': 'Голубое',
                            'BS': 'Голубое - Solar Control',
                            'BZ': 'Бронза',
                            'CB': 'Прозрачное/Теплопоглощающее',
                            'CC': 'Прозрачное/Теплоотражающее',
                            'CD': 'Прозрачное/Теплоотражающее + Шумоизоляция',
                            'CL': 'Прозрачное',
                            'GA': 'Зеленое + Шумоизоляция',
                            'GB': 'Зеленое/Теплопоглощающее',
                            'GS': 'Зеленое - Solar Control',
                            'GY': 'Серое',
                            'LG': 'Светло Зеленое',
                            'YA': 'Серое + Шумоизоляция',
                            'YC': 'Серое/Теплоотражающее',
                            'YS': 'Серое - Solar Control',
                            'ZS': 'Бронзовое - Solar Control'
                        };
                        var vehicleData = {
                            'C': 'Купе',
                            'H': 'Хетчбек',
                            'S': 'Седан',
                            'E': 'Комби',
                            'M': 'MPV (*)',
                            'R': 'Ranger',
                            'T': 'Кабриолет',
                            'L': 'Грузовик',
                            'V': 'Минивен',
                            'P': 'Пикап'
                        };

                        textVehicleBodyType = 'Vehicle Body Type';


                    }
                    else if ($.inArray(getType, windShieldWord) != -1) {

                        var colorData = {
                            'GN': 'Зелёное',
                            'AB': 'Прозрачное противоударное (5 слоев)',
                            'AS': 'Прозначное противоударное (3 слоя)',
                            'BA': 'Голубое+Шумоизоляция',
                            'BB': 'Голубое/Теплопоглощающее',
                            'BL': 'Голубое',
                            'BS': 'Голубое - Solar Control',
                            'BZ': 'Бронза',
                            'CB': 'Прозрачное/Теплопоглощающее',
                            'CC': 'Прозрачное/Теплоотражающее',
                            'CD': 'Прозрачное/Теплоотражающее + Шумоизоляция',
                            'CL': 'Прозрачное',
                            'GA': 'Зеленое + Шумоизоляция',
                            'GB': 'Зеленое/Теплопоглощающее',
                            'GS': 'Зеленое - Solar Control',
                            'GY': 'Серое',
                            'LG': 'Светло Зеленое',
                            'YA': 'Серое + Шумоизоляция',
                            'YC': 'Серое/Теплоотражающее',
                            'YS': 'Серое - Solar Control',
                            'ZS': 'Бронзовое - Solar Control'
                        };
                        var vehicleData = {
                            'BABL': 'Голубое + Шумоизоляция/Голубая полоса',
                            'BAGN': 'Голубое + Шумоизоляция/Зеленая полоса',
                            'BBBL': 'Голубое Теплопоглощающее/Голубая полоса',
                            'BBGN': 'Голубое Теплопоглощающее/Зеленая полоса',
                            'BBYN': 'Голубое Теплопоглощающее/Серая полоса',
                            'BLBL': 'Голубое /Голубая полоса',
                            'BLGN': 'Голубое /Зеленая полоса',
                            'BSBL': 'Голубое Solar Control/Голубая полоса',
                            'BSGN': 'Голубое Solar Control/Зеленая полоса',
                            'BSGY': 'Голубое Solar Control/Серая полоса',
                            'BZBZ': 'Бронзовое/Бронзовая полоса',
                            'BZGN': 'Бронзовое/Зеленая полоса',
                            'BZBL': 'Бронзовое/Голубая полоса',
                            'BZGY': "Бронзовое/Серая полоса",
                            'CBBL': 'Прозрачное Поглощающее IR и UV/Голубая полоса',
                            'CBBZ': 'Прозрачное Поглощающее IR и UV/Бронзовая полоса',
                            'CBGN': 'Прозрачное Поглощающее IR и UV/Зеленая полоса',
                            'CBGY': "Прозрачное Поглощающее IR и UV/Серая полоса",
                            'CCBL': 'Прозрачное/Теплоотражающее/Голубая полоса',
                            'CCBZ': 'Прозрачное/Теплоотражающее/Бронзовая полоса',
                            'CCGN': 'Прозрачное/Теплоотражающее/Зеленая полоса',
                            'CCGY': 'Прозрачное/Теплоотражающее/Серая полоса',
                            'CDBL': 'Прозрачное/Теплоотражающее+Шумоизоляция/Голубая полоса',
                            'CDBZ': 'Прозрачное/Теплоотражающее+Шумоизоляция/Бронзовая полоса',
                            'CDGN': 'Прозрачное/Теплоотражающее+Шумоизоляция/Зеленая полоса',
                            'CDGY': 'Прозрачное/Теплоотражающее+Шумоизоляция/Серая полоса',
                            'CLBL': 'Прозрачное/Голубая полоса',
                            'CLBZ': 'Прозрачное/Бронзовая полоса',
                            'CLGN': 'Прозрачное/Зеленая полоса',
                            'CLGY': 'Прозрачное/Серая полоса',
                            'GBBL': 'Зеленое Поглощающее IR и UV/Голубая полоса',
                            'GBBZ': 'Зеленое Поглощающее IR и UV/Бронзовая полоса',
                            'GBGN': 'Зеленое Поглощающее IR и UV/Зеленая полоса',
                            'GBGY': 'Зеленое Поглощающее IR и UV/Серая полоса',
                            'GABL': 'Зеленое + Шумоизоляция/Голубая полоса',
                            'GABZ': 'Зеленое + Шумоизоляция/Бронзовая полоса',
                            'GAGN': 'Зеленое + Шумоизоляция/Зеленая полоса',
                            'GAGY': 'Зеленое + Шумоизоляция/Серая полоса',
                            'GNBL': 'Зеленое/Голубая полоса',
                            'GNBZ': 'Зеленое/Бронзовая полоса',
                            'GNGN': 'Зеленое/Зеленая полоса',
                            'GNGY': 'Зеленое/Серая полоса',
                            'GSBL': 'Зеленое Solar Control/Голубая полоса',
                            'GSBZ': 'Зеленое Solar Control/Бронзовая полоса',
                            'GSGN': 'Зеленое Solar Control/Зеленая полоса',
                            'GSGY': 'Зеленое Solar Control/Серая полоса',
                            'GSYD': 'Зеленое Solar Control/Темно-Серая полоса',
                            'GYBL': 'Серое/Голубая полоса',
                            'GYGY': 'Серое/Серая полоса',
                            'LGBL': 'Светло Зеленое/Голубая полоса ',
                            'LGLG': 'Светло Зеленое/Светло Зеленая полоса ',
                            'YABL': 'Серое + Шумоизоляция/Голубая полоса',
                            'YAGY': 'Серое + Шумоизоляция/Серая полоса',
                            'YSBL': 'Серое Solar Control/Голубая полоса',
                            'YSGY': 'Серое Solar Control/Серая полоса'
                        };
                        textVehicleBodyType = 'Цвет Полосы';
                        var accessoriesData = {
                            'A': 'Антенна',
                            'B': 'Правый руль',
                            'C': 'Место для камеры ночьного видения/или предупреждения переулка Функциональные возможности ACC',
                            'G': 'GPS',
                            'H': 'С обогревом',
                            'J': 'ТВ Антенна',
                            'M': 'Датчик (Света и/или Дождя)',
                            'O': 'Датчик тумана',
                            'V': 'VIN окно',
                            'U': 'Дисплей для характеристик'
                        };

                    }
                    ;
                    for (var z = 0; z < filterData[0].length; z++) {

                        if(filterData[0][z] == filterData[0][z-1]) continue;
                        var el = filterData[0][z];
                        var datColor = colorData[el];
                        var flagContinue = true;
                        var paramElems = $('.dopcollor');
                        for (var q = 0; q < paramElems.length; q++) {
                            if (paramElems[q].value == datColor) {
                                flagContinue = false;
                                console.log('совпало:' + datColor);
                                break
                            }
                        }

                        if (flagContinue == false) continue;
                        filterCount = z + 1;
                        //$(paramFilter).before('<label for="data{1}" data-id="filterColor' + type + '"><p>Цветное стекло</p><div class="subline"></div></label>')
                        // $('label[data-id="filterColor' + type + '"]').find('.subline').append('<label for="data{1}{' + filterCount + '}{' + type + '}"><input type="radio" data-type="data-color" data-filter="' + el + '" name="data{1}" id="data{1}{' + filterCount + '}{' + type + '}"><p class="colorName">' + datColor + '</p></label>')
                        //  });
                        if (z == 0){ $(paramFilter).append('<div class="col-md-3 col-sm-6 colorcheck"><div class="row"><label for="q01" class="col-xs-12 col-md-12 col-sm-12"><input type="checkbox" name="color" id="q01" ><p>цветное стекло</p></label></div>')}
                        $('.colorcheck .row').append('<label for="q01'+ z +'" class="col-xs-12 col-sm-6"><input class="dopcollor" type="radio" value="'+ datColor +'" name="dopcollor" id="q01'+ z +'"><p>'+ datColor +'</p></label>');
                    }


                    for (var z = 0; z < filterData[1].length; z++) {
                        //filterData[1].forEach(function(el, i) {
                        if(filterData[1][z] == filterData[1][z-1]) continue;
                        var el = filterData[1][z];
                        var datVehicle = vehicleData[el];
                        var flagContinue = true;
                        var paramElems = $(paramFilter).find('.bodyType');
                        for (var q = 0; q < paramElems.length; q++) {
                            if (paramElems[q].innerHTML == datVehicle) {
                                flagContinue = false;
                                console.log('совпало:' + datVehicle);
                                break
                            }
                        }
                        if (flagContinue == false) continue;
                        filterCount = z + 1;
                        if (z == 0) { $(paramFilter).append('<div class="col-md-3 col-sm-6 colorsolidcheck"><div class="row"><label for="q031" class="col-xs-12 col-md-12 col-sm-12"><input type="checkbox" name="band" id="q031" ><p>' + textVehicleBodyType + '</p></label></div>')}
                        $('.colorsolidcheck .row').append('<label for="cc'+ z +'" class="col-xs-12 col-sm-6"><input class="dopcollor" type="radio" value="'+ datVehicle +'" name="dopsolidcollor" id="cc'+ z +'"><p>'+ datVehicle +'</p></label>');

                         }

                    //  filterData[2].forEach(function(el, i) {
                    // for (var z = 0; z < filterData[2].length; z++) {
                    //     if(filterData[2][z] == filterData[2][z-1]) continue;
                    //     var el = filterData[2][z];
                    //     var datPrioritet = el;
                    //     var flagContinue = true;
                    //     var paramElems = $(manufacturerFilter).find('.manufactureName');
                    //     for (var q = 0; q < paramElems.length; q++) {
                    //         if (paramElems[q].innerHTML == datPrioritet) {
                    //             flagContinue = false;
                    //             console.log('совпало:' + datPrioritet);
                    //             break
                    //         }
                    //     }
                    //     if (flagContinue == false) continue;
                    //     filterCount = z + 1;
                    //     var minPrice = filterData[3][el];
                    //     var marja = filterData[3][el + '_marja'];
                    //     // $(manufacturerFilter).before('<label for="manufacture{' + z + '}{' + type + '}" data-id="filterManufacture"><input type="checkbox" data-type="data-manufacturer_' + z + '" data-filter="' + el + '" name="" id="manufacture{' + z + '}{' + type + '}"><p class="manufactureName">' + el + '</p><span>Маржа ' + marja + '</span><span>Цены от ' + minPrice + '</span></label>');
                    // }
                    //  });
                    var prioritetCount = 0;
                    //    filterData[4].forEach(function(el, i) {
                    for (var z = 0; z < filterData[4].length; z++) {

                        var stop = 0;
                        for(var i = 0; i < z;i++ ){
                            if(filterData[4][i]==filterData[4][z]){
                                stop = 1;
                            }
                        }
                        if( stop == 1 ) continue;
                        var el = filterData[4][z];
                        var datPrioritet = el;
                        var flagContinue = true;
                        var paramElems = $('.prioritet');
                        for (var q = 0; q < paramElems.length; q++) {
                            if (paramElems[q].value == datPrioritet) {
                                flagContinue = false;
                                console.log('совпало:' + datPrioritet);
                                break
                            }
                        }
                        if (flagContinue == false) continue;
                        filterCount = z + 1;
                        var getMin = function (prioritet) {
                            var mas = [];

                            if (type == 'searchCar') {
                                var tab = table;
                            }
                            else if (type == 'searchEurocode') {
                                var tab = table;
                            }

                            $(tab).find('tr[data-prioritet="' + prioritet + '"]').each(function (i, elem) {

                                var coast = +($(elem).attr('data-price'));
                                mas.push(coast);

                            })

                            return mas;
                        }(el)
                        var minCoastPrioritet = Math.min.apply(Math, getMin);
                        var title = '';
                        if(el == 'budget'){
                            title = 'Бюджетное стекло';
                        }else if(el == 'econom'){
                            title = 'Экономный вариант';
                        }else if(el == 'premium'){
                            title = 'Премиум стекло';
                        }else if(el == 'business'){
                            title = 'Бизнес стекло';
                        }
                        $('.radioblock').append('<div class="col-md-3 item"><label for="t'+ z +'" ><input class="prioritet" value="'+ el +'" type="radio" data-type="data-prioritet_' + z + '" name="tarif" id="t'+ z +'"  "><p class="title">'+ title +'</p><p class="price">Цены от '+ minCoastPrioritet +' Р</p><div class="text"><p>Это всемирно известные бренды с многолетней историей и хорошей репутацией проверенных поставщиков на автоконвейеры. К этой ценовой категории относят стекла AGC, Pilkington и Securit. Продукция Pilkington и Securit полностью импортная, а AGC частично производятся на Борском Стекольном Заводе в Нижегородсткой области.</p><a href=""><span>Подробнее</span></a></div><div class="img"><img src="<?php bloginfo(\'template_directory\')?>/img/brand1.png" alt=""><img src="<?php bloginfo(\'template_directory\')?>/img/brand2.png" alt=""></div></label></div>');
                        // .before('<label for="prioritet{' + z + '}{' + type + '}" data-id="filterPrioritet"><input type="checkbox" data-type="data-prioritet_' + z + '" data-filter="' + el + '" name="" id="prioritet{' + z + '}{' + type + '}"><p class="prioritetName">' + el + '</p><span>цены от :' + minCoastPrioritet + '</span></label>');
                    }
                    //  });
                    if ($.inArray(getType, sideWindowWord) != -1) {
                        for (var z = 0; z < filterData[5].length; z++) {
                            if(filterData[5][z] == filterData[5][z-1]) continue;

                            var el = filterData[5][z];
                            var flagContinue = true;
                            var paramElems = $('.leftrightpos');
                            for (var q = 0; q < paramElems.length; q++) {

                                if (paramElems[q].value == el) {
                                    flagContinue = false;
                                    console.log('совпало:' + el);
                                    break
                                }

                            }
                            if (flagContinue == false) continue;
                            filterCount = z + 1;
                            if (z == 0) {
                                $(paramFilter).append('<div class="col-md-3 col-sm-6 leftrightposition"></div>');
                            }
                            $('.leftrightposition').append('<div class="row"><label for="leftrightposition'+ z +'" class="col-xs-12 col-sm-12"><input class="leftrightpos" type="checkbox" value="' + el + '" name="' + el + '" id="leftrightposition'+ z +'"><p>' + el + '</p></label></div>');

                        }
                        //  filterData[6].forEach(function(el, i) {
                        for (var z = 0; z < filterData[6].length; z++) {
                            if(filterData[6][z] == filterData[6][z-1]) continue;
                            var el = filterData[6][z];
                            var datPrioritet = glassPositionData[el];
                            var flagContinue = true;
                            var paramElems = $('.glasspos');
                            for (var q = 0; q < paramElems.length; q++) {
                                if (paramElems[q].value == datPrioritet) {
                                    flagContinue = false;
                                    console.log('совпало:' + datPrioritet);
                                    break
                                }
                            }
                            if (flagContinue == false) continue;
                            filterCount = z + 1;
                            if (z == 0) {
                                $(paramFilter).append('<div class="col-md-3 col-sm-6 glassposition"></div>');
                            }
                            $('.glassposition').append('<div class="row"><label for="position'+ z +'" class="col-xs-12 col-sm-12"><input class="glasspos" type="checkbox" value="' + glassPositionData[el] + '" name="' + glassPositionData[el] + '" id="position'+ z +'"><p>' + glassPositionData[el] + '</p></label></div>');
                            // $('label[data-id="filterGlassPosition{' + type + '}"]').find('.subline').append('<label for="data{3}{' + filterCount + '}{' + type + '}"><input type="radio" data-type="data-glassPosition" data-filter="' + el + '" name="data{3}" id="data{3}{' + filterCount + '}{' + type + '}"><p class="glassPositionName">' + glassPositionData[el] + '</p></label>');
                        }
                        //  });
                    }
                    ;

                    if ($.inArray(getType, windShieldWord) != -1) {
                        var lastMas = filterData[filterData.length - 1];
                        for (var z = 0; z < lastMas.length; z++) {

                            var el = lastMas[z];
                            if( lastMas[z] ==  lastMas[z-1]) continue;
                            filterCount = z + 1;
                            if (z == 0) $(paramFilter).append('<div class="col-md-3 col-sm-6 accessories"></div>');
                            if (accessoriesData[el] == undefined) continue;
                            $('.accessories').append(' <div class="row"><label for="acces'+ z +'" class="col-xs-12 col-sm-12"><input type="checkbox" name="' + accessoriesData[el] + '" id="acces'+ z +'"><p>' + accessoriesData[el] + '</p></label></div>');
                        }

                    }

                }
            })

            $(parentElem).find('input').bind('change', filterParam);
            $(parentElem).find('input').bind('change', getLocalData);
            //Ставим фильтры
            function setJqueryId(str) {
                for (var i = 0; i < str.length; ++i) {
                    if (str[i] != '{' && str[i] != '}') strN += str[i]
                    else strN += "\\" + str[i];
                }
            }

            // if (localData.filter() != null && autoStatus) {
            //     var masFilter = JSON.parse(localData.filter());
            //     var strN = '';
            //     $(masFilter).each(function (i, el) {
            //         var id = el.id;
            //         strN = '';
            //         try {
            //             setJqueryId(id);
            //             $('#' + strN).prop('checked', true);
            //             filterParam()
            //         }
            //         catch (err) {
            //
            //         }
            //
            //     })
            //
            //
            // }
            // ;

            // if (localData.offerItems() != null) {
            //
            //     $(JSON.parse(localData.offerItems())).each(function (i, elem) {
            //         strN = '';
            //         setJqueryId(elem)
            //         $('#' + strN).prop('checked', true);
            //         console.log($('#' + strN));
            //         console.log($('#' + strN).prop('checked'));
            //         var event = new Event('change');
            //         var element = document.getElementById(elem);
            //         try {
            //             element.dispatchEvent(event);
            //         } catch (err) {
            //         }
            //         ;
            //
            //     })
            //
            //
            // }
            function filterParam() {

                var filterMas = {};
                var allFilters = $(parentElem).find('input[data-filter]:checked');
                var mainFilters = {};
                var mainAttributes = '';
                var attributes = '';
                var elemMas = [];
                var flag = true;
                var mainFlag = false;
                $(allFilters).each(function (i, el) {

                    if ($(el).attr('data-type') == 'data-color' || $(el).attr('data-type') == 'data-type' || $(el).attr('data-type') == 'data-glassPosition' || $(el).attr('data-type') == 'data-typeof' || $(el).attr('data-type').split('_')[0] == 'data-accesories') {

                        mainFilters[$(el).attr('data-type')] = $(el).attr('data-filter');
                        mainFlag = true;
                    }
                    else {
                        flag = false;
                        mainFlag = true;
                        filterMas[$(el).attr('data-type')] = $(el).attr('data-filter');
                    }

                });

                if (type == 'searchCar') {
                    var allTr = $('table[data-id="table_eurocode_searchCar"] > tbody > tr');
                    var table = $('table[data-id="table_eurocode_searchCar"]');
                    //определяем какие данные группировать
                    var activeTdHead = $('.tablesorter-headerRow').find('.active').attr('data-name');
                    var groupElemParam;
                    if (activeTdHead == 'tdHeadErucode') groupElemParam = '.eurocodeTd'
                    else if (activeTdHead == 'tdHeadManufacture') groupElemParam = '.manufacturerTd'
                    else if (activeTdHead == 'tdHeadDiller') groupElemParam = '.dillerTd';
                    else if (activeTdHead == 'tdHeadYc') groupElemParam = '.ycTd';
                    var groupElem = function () {
                        if (activeTdHead != 'tdHeadManufacture') groupElements($('.viewElems').find(groupElemParam), tBody_eurocode_searchCar, groupElemParam)
                        else groupElements($('.viewElems').find(groupElemParam), tBody_eurocode_searchCar, '.manufacturerTd', true)
                    };
                }
                else if (type == 'searchEurocode') {
                    var allTr = $('table[data-id="table_eurocode"] > tbody > tr');
                    var table = $('table[data-id="table_eurocode"]');
                    //определяем какие данные группировать
                    var activeTdHead = $('.tablesorter-headerRow').find('.active').attr('data-name');
                    var groupElemParam;
                    if (activeTdHead == 'tdHeadErucode') groupElemParam = '.eurocodeTd'
                    else if (activeTdHead == 'tdHeadManufacture') groupElemParam = '.manufacturerTd'
                    else if (activeTdHead == 'tdHeadDiller') groupElemParam = '.dillerTd';
                    else if (activeTdHead == 'tdHeadYc') groupElemParam = '.ycTd';
                    //  var groupElem = function() { groupElements($('.viewElems').find(groupElemParam), tBody_eurocode, groupElemParam); };
                    var groupElem = function () {
                        if (activeTdHead != 'tdHeadManufacture') groupElements($('.viewElems').find(groupElemParam), tBody_eurocode, groupElemParam)
                        else groupElements($('.viewElems').find(groupElemParam), tBody_eurocode, '.manufacturerTd', true)
                    };
                }

                if (mainFlag == false) {
                    $(allTr).show();
                    var viewElems = $(table).find('tbody').find('tr:visible');
                    $(table).find('tr').removeClass('viewElems');
                    $(viewElems).addClass('viewElems');
                    //по default eurocode
                    groupElem();
                    return;
                }

                for (var key in mainFilters) {
                    if (key.split('_')[0] == 'data-accesories') mainAttributes += '[' + key.split('_')[0] + '*=' + '"' + mainFilters[key] + '"' + ']';
                    else mainAttributes += '[' + key + '=' + '"' + mainFilters[key] + '"' + ']';
                    var el = $(table).find('tbody > tr' + mainAttributes + '');
                    //elemMas.push(el);
                }

                if (flag == false) {

                    $(allTr).each(function (i, elem) {

                        $(elem).hide();

                    })

                    for (var key in filterMas) {

                        var splitKey = key.split('_');

                        attributes = mainAttributes + '[' + splitKey[0] + '=' + '"' + filterMas[key] + '"' + ']';
                        var el = $(table).find('tbody > tr' + attributes + '');
                        elemMas.push(el);


                    }
                    ;

                    $(elemMas).each(function (i, elem) {

                        $(elem).show();

                    })
                }
                else {

                    $(allTr).each(function (i, elem) {

                        $(elem).hide();

                    })

                    $(el).show();
                }
//СЮДА вставляем функцию ГРУппировки
//нужно сделать выборку всех tr которые видны
                var viewElems = $(table).find('tbody').find('tr:visible');
                $(table).find('tr').removeClass('viewElems');
                $(viewElems).addClass('viewElems');
                //по default eurocode
                groupElem();
                //groupElements($('.viewElems').find('.eurocodeTd'), tBody_eurocode_searchCar,'.eurocodeTd');
            }

            function getLocalData() {

                //Для автозполнения по радио кнопкам

                if ($(this).prop('checked') && $(this).attr('type') != 'checkbox') {
                    var flagFilter = false;
                    var attr = $(this).parents('label[for="' + $(this).attr('name') + '"]').attr('data-id');
                    var id = $(this).attr('id');
                    $(localFilterMas).each(function (i, el) {
                        if (el.parent == attr) {
                            el.id = id;
                            flagFilter = true
                        }
                    })
                    if (!flagFilter) localFilterMas.push({
                        parent: $(this).parents('label[for="' + $(this).attr('name') + '"]').attr('data-id'),
                        id: id
                    });
                }

                //Для автозаполнения по чекбоксам при активности
                else if ($(this).prop('checked') && $(this).attr('type') == 'checkbox') {
                    var id = $(this).attr('id');
                    localFilterMas.push({parent: 'checkbox', id: id});
                }

                else if ($(this).prop('checked') == false && $(this).attr('type') == 'checkbox') {
                    var id = $(this).attr('id');
                    $(localFilterMas).each(function (i, el) {
                        if (el.id == id) {
                            localFilterMas.splice(i, 1)
                        }
                    })

                }
                localStorage.setItem('filter', JSON.stringify(localFilterMas));
            }
            for(var i = 0; i < filterData.length; i++ ){

            }
        };
//         var str = '';
//         for (var i = 0 ; i < masDopInfo.length; i++) {
// str += '<tr><input type="checkbox"><p> '+ masDopInfo[i] +'</p></tr>';
//         }
//         $(checkblock).append(str);
        $(checkblock).append('<div class="col-md-3 col-sm-6"><div class="row"><input id="reset" type="reset" value="Сбросить фильтр"></div></div>');

        if ($("div").is("#installation")) {
            console.log('+');
        }else{
            $('.res_search').before('<div id="installation"><h4>Понадобиться ли Вам установка?</h4><label for="n1" class="ln col-xs-12 col-md-3 col-md-offset-3"><input type="radio" checked value="yes" name="n" id="n1"><p>Да, установите мне стекло</p></label><label for="n2" class="ln col-xs-12 col-md-3"><input value="no" type="radio" name="n" id="n2"><p>Нет, я займусь этим сам</p></label></div>');
        }
        $('body').on("click", '#reset', function (){
            $('.res_search').find('h3').text('мы нашли для вас '+ countMyObj  +' стекол(ла)');
            $('.none').css('display','block');
            $('.result > div').removeClass('none');
            masDisplayBlock=[];
        });
        $('#installation').on("click", 'label', function (){
            var price;
           for(var i=0;i<countMyObj;i++){
               if($('#installation input')[0].checked == true){
                   price = myObj[i].sum_cost;
               }else{
                   price = myObj[i].sum_cost_wi;
               }
               $('#'+i+'').find('.price').text(price);
           }
        });
            $('body').on("click", 'label', function (){
                $('.res_search').find('h3').text('мы нашли для вас '+ countMyObj  +' стекол(ла)');
                $('.none').css('display','block');
                $('.result > div').removeClass('none');
                masDisplayBlock = [];
                var valuecheckbox;
                var countInput = $('.filterparam input').length;
                for(var j=0;j<countInput;j++){
                    if($('.filterparam input')[j].checked == true) {
                        if($('.filterparam input')[j].type=='checkbox'){
                            valuecheckbox = $('.filterparam input')[j].name;
                        }else{
                            valuecheckbox = $('.filterparam input')[j].value;
                        }
                        for(i=0;i<massGlass.length;i++){
                            var display = 0;
                            if(valuecheckbox=='budget' || valuecheckbox=='econom' || valuecheckbox=='business' ||valuecheckbox=='premium' ) {
                                var dataSegment = $('#'+i+'').attr('data-segment');
                                if(dataSegment == valuecheckbox) {
                                    display = 1;
                                }
                            }
                            for(key in massGlass[i]){

                                if(valuecheckbox=='color' || valuecheckbox=='R' || valuecheckbox=='L' || valuecheckbox=='band'){
                                    if(key==valuecheckbox){
                                        display = 1;
                                    }
                                }else{
                                    if(massGlass[i][key]==valuecheckbox){
                                        display = 1;
                                    }
                                }
                            }
                            if(display==0){
                                $('#'+i+'').addClass('none');
                                $('.none').css('display','none');
                                masDisplayBlock.push(i);
                            }

                        }
                        $('.res_search').find('h3').text('мы нашли для вас '+ (countMyObj - $(".none").length) +' стекол(ла)');
                    }

                }
            });
        $('body').on('click', '.sort', function (e) {
            e.preventDefault();
            var param = $(this).data('param');
            sortItem(param);
        });
        function sortItem(parametr) {
            var resultblock = $('.result');
            resultblock.html(' ');
            function comparesum(objectA, objectB) {
                console.log(objectB.sum_cost);
                console.log(objectA.sum_cost);
                return (objectB.sum_cost - objectA.sum_cost);
            }
            function comparemanufacturer(objectA, objectB) {
                if (objectA.manufacturer_name > objectB.manufacturer_name) {
                        return 1;
                }
                if (objectA.manufacturer_name < objectB.manufacturer_name) {
                        return -1;
                }
                        return 0;
            }
            function compareItem(objectA, objectB) {
                return (objectA.parametr - objectB.parametr);
            }
            function compareItem(objectA, objectB) {
                return (objectA.parametr - objectB.parametr);
            }
            var masSortItem = myObj;
            // for(var i=0;i<masSortItem.length;i++) {
                for(var j = 0;j < masDisplayBlock.length; j++) {
                        masSortItem[masDisplayBlock[j]].display = 'none';
                }
            // }
          if(parametr=='comparesum'){
              masSortItem.sort(comparesum);
          }else if(parametr=='comparemanufacturer'){
              masSortItem.sort(comparemanufacturer);
          }

            for(var i = 0; i < masSortItem.length; i++) {
                $('.result').append('<div class="item  ' + masSortItem[i].display+'" data-segment="' + masSortItem[i].manufacturer_segment + '" id="'+ i +'"><div class="clickable open"><div class="col-md-2 col-sm-3 col-xs-12 img"><a href="#" class="show"></a><img src="<?php bloginfo(\'template_directory\')?>/img/glass1.png" alt=""></div> <div class="col-md-5 col-sm-5 col-xs-12 title"><strong>' + masSortItem[i].name + '</strong>' + ' <p>' + content_f + '</p>' + '</div><div class="col-md-3 col-sm-4  col-xs-6 rating"><p>Рейтинг <b>4.50'+masSortItem[i].manufacturer_name+'</b><br/><span>325 оценок</span></p><p class="label">Нет в наличии</p> </div><div class="col-md-2 col-sm-12 col-xs-6 "><div class="col-md-12 col-sm-6"> <p class="price old">' + masSortItem[i].sum_crossed + '</p><p class="price">' + masSortItem[i].sum_cost + '</p></div><div class="col-md-12 col-sm-6"><button class="btn-primary">Выбрать</button><div class="text-center"><a href="#" class="btn-compare"></a><a href="#" class="btn-star"></a></div></div> </div></div><div class="col-sm-12 body"><div class="row"><div class="col-md-5 col-md-offset-2 tabs"><ul class="nav nav-tabs" id="blogPages"><li class="active"><a data-toggle="tab" href="#tb11'+ i +'">Характеристики</a></li><li><a data-toggle="tab" href="#tb12'+ i +'">Датчики</a></li></ul><div class="tab-content"><div class="tab-pane fade in active" id="tb11'+ i +'"><table><tr><td>Размеры стекла</td><td>0x0</td></tr><div class="tab-pane fade" id="tb12'+ i +'"><table><tr><td>Размеры стекла</td><td>1409ч673</td></tr></table></div></div></div><div class="col-md-3 text"><strong>'+ masSortItem[i].manufacturer_name + '</strong><p>'+ masSortItem[i].manufacturer_description+'</p> <button class="btn-primary">О производителе <i></i></button></div></div></div></div>');

            }
            $('.none').css('display','none');

        }
    }
})(jQuery);

// (function ($) {
//     $(function () {
//         console.log('asasasasas');
//         var masMinMax;
//         var itemObj;
//
//         var table_eurocode = $('table[data-id="table_eurocode"]');
//         var table_eurocode_searchCar = $('table[data-id="table_eurocode_searchCar"]');
//         var tBody_eurocode = $(table_eurocode).find('tbody');
//         var tBody_eurocode_searchCar = $(table_eurocode_searchCar).find('tbody');
//         var input_eurocode = $('#eurocode').find('.form-control');
//         var globalCount = 0;
//         var localFilterMas = [];
//         var localOfferMas = [];
//         var autoStatus;
// //глобальна функция группировки элементов
//
//         function groupElements(elems, tBody, tdClass, manufac, group, childs) {
//
//             var masText = [];
//             if (!group) {
//                 var allTr = $(tBody).find('.viewElems')//$(tBody).find('tr');
//                 //clear css
//                 $(allTr).removeClass('activeGroupTwo').removeClass('activeGroupOne');
//                 $(allTr).show();
//                 $(allTr).removeClass('parentGroup');
//                 $(allTr).removeClass('childGroup');
//                 $(allTr).find(tdClass).removeClass('activeTdElem');
//                 $(allTr).find(tdClass).removeClass('reActiveTdElem');
//                 $(allTr).find('.activeParentTd').off();
//                 $(allTr).find('td').removeClass('activeChildTd');
//                 $(allTr).attr('data-class', '');
//                 $(tBody).find('tr').attr('data-class', '');
//                 $(tBody).find('tr').attr('data-classTwoGroup', '');
//                 $('td').removeClass('activeParentTd')
//             }
//             else {
//                 var allTr = childs;
//             }
//             //перебираем элементы, заносим их текст в массив;
//             $(elems).each(function (i, el) {
//                 masText.push($(el).text());
//             })
//             //создаем массив для совпадений
//             var newMas = {};
//             //перебираем masText, ищем совпадения
//             $(masText).each(function (i, elText) {
//                 var transitMas = [];
//                 var price = [];
//                 for (var k = i + 1; k < masText.length; k++) {
//                     var nextElemText = masText[k];
//                     if (elText == nextElemText) {
//                         transitMas.push(k);
//                         masText.splice(k, 1, 'text' + i + k);
//                         flag = true;
//                     }
//                 }
//                 ;
//
//                 if (transitMas.length) {
//                     //добавляем элемент от которого искали совпадения
//                     transitMas.push(i);
//                     //в transitMas лежит список совпадений, далее проходим по этому списку и закидываем price
//                     var priceMas = [];
//                     $(transitMas).each(function (i, el) {
//                         if (!group) var priceFrom = $(tBody).find('tr:visible').eq(el).find('.priceFrom').text();
//                         else var priceFrom = $(allTr).eq(el).find('.priceFrom').text();
//
//                         priceFrom = Number(priceFrom);
//                         priceMas.push(priceFrom);
//                     })
//                     //находим наименьшее в массиве
//                     var mPrice = Array.min(priceMas);
//                     //находим index элемента с минимальной стоимостью
//                     var indexMin = (function getIndex() {
//                         var returnItem;
//                         $(priceMas).each(function (index, el) {
//                             if (el == mPrice) returnItem = index;
//                         })
//                         return returnItem;
//                     })();
// //Находим детей
//                     var childMas = (function () {
//                         var mas = [];
//                         $(transitMas).each(function (i, el) {
//                             if (i != indexMin) mas.push(el)
//                         })
//                         return mas;
//                     })()
//
//                     newMas[elText] = {'elems': transitMas, prices: priceMas, minPrice: indexMin, childs: childMas};
//                     //   console.log(priceMas)
//                 }
//                 ;
//             })
//             console.log(newMas);
//
//
//             //функция выборки элементов из объекта
//             function massivElements() {
//                 var mas = [];
//                 for (var key in newMas) {
//                     mas.push(newMas[key].elems)
//                 }
//                 ;
//                 return mas;
//             }
//
//             (function changeElements() {
//                 var countElems = 0;
//                 for (var key in newMas) {
//                     //проходим по ключаем объекта и выбираем parentElement
//                     var parentElement = newMas[key].elems[newMas[key].minPrice];
//                     var parentElementTr = $(allTr).eq(parentElement);
//
//                     $(allTr).eq(parentElement).addClass('parentGroup');
//                     $(allTr).eq(parentElement).addClass('activeGroup');
//                     //  if(group) $(allTr).eq(parentElement).removeClass('childGroup') && $(allTr)eq(parentElement).addClass('childGroupOneParent')
//                     $(allTr).eq(parentElement).find(tdClass).addClass('activeTdElem');
//                     if (group) $(allTr).eq(parentElement).find(tdClass).addClass('twoGroup');
//                     $(newMas[key].elems).each(function (ind, e) {
//                         $(allTr).eq(e).insertAfter(parentElementTr)
//                         if (!group) $(allTr).eq(e).attr('data-class', 'group' + countElems);
//                         else {
//                             var first = $(allTr).eq(e).attr('data-class');
//                             $(allTr).eq(e).attr('data-classTwoGroup', first + countElems);
//                         }
//                     })
//
//                     //перебираем деток
//                     $(newMas[key].childs).each(function (ind, e) {
//                         $(allTr).eq(e).addClass('childGroup');
//                     })
//                     // console.log($(elems).eq(parentElement));
//                     countElems++;
//                 }
//             })();
//             //передвигаем элементу друг к другу
//
//             if (!group) $('.childGroup').hide();
//             else {
//                 $(allTr).each(function (i, el) {
//                     if ($(el).attr('data-classTwoGroup') && !$(el).hasClass('parentGroup')) $(el).hide();
//                 })
//             }
//             var clickFlag = true;
//             //Не нужно вешать обрабочтик события на paentGroup
//             $('.parentGroup').find(tdClass).addClass('activeParentTd');
//             $('.childGroup').find(tdClass).addClass('activeChildTd');
//             //$(tdClass).off();
//             if (!group) $('.parentGroup').find(tdClass).off().on('click', showTrChild)
//             else $('.twoGroup').off().on('click', anotherFunc);
//
//             function anotherFunc() {
//                 var dataAtr = $(this).parent().attr('data-classtwogroup');
//                 var childElems = $(this).parent().find('~[data-classtwogroup="' + dataAtr + '"]');
//                 if (!$(this).parent().hasClass('activeGroup')) {
//                     // $(this).parent().addClass('activeGroupOne');
//                     //   $(this).parent().removeClass('activeGroupTwo');
//                     $(childElems).addClass('activeGroupOne');
//                     $(childElems).removeClass('activeGroupTwo');
//                     $(this).removeClass('reActiveTdElem');
//                     $(this).addClass('activeTdElem');
//                     $(this).parent().addClass('activeGroup');
//                     $(childElems).hide()
//                 }
//                 else {
//                     //   $(this).parent().removeClass('activeGroupOne');
//                     //   $(this).parent().addClass('activeGroupTwo');
//                     $(childElems).removeClass('activeGroupOne');
//                     $(childElems).addClass('activeGroupTwo');
//                     //   $(this).parent().addClass('activeGroupTwo');
//
//                     $(this).removeClass('activeTdElem');
//                     $(this).addClass('reActiveTdElem');
//                     $(this).parent().removeClass('activeGroup');
//                     $(childElems).show()
//                 }
//             }
//
//             function showTrChild() {
//
//                 var dataAtr = $(this).parent().attr('data-class');
//                 var flagThirdFunc = true;
//                 if (!$(this).parent().hasClass('activeGroup')) {
//                     //css
//                     $(this).parent().removeClass('activeGroupOne');
//
//                     $(this).removeClass('reActiveTdElem');
//                     $(this).addClass('activeTdElem');
//                     $(this).parent().addClass('activeGroup');
//                     var parentElem = $(this).parent();
//                     var parentElementAtr = $(parentElem).attr('data-class');
//                     var childs = $('.childGroup[data-class="' + dataAtr + '"]').find('.manufacturerTd');
//                     var childTdParentElem = $(parentElem).find('.manufacturerTd');
//                     var childsTr = $('.childGroup[data-class="' + dataAtr + '"]');
//                     //css
//                     $(childsTr).removeClass('activeGroupOne');
//                     $('.childGroup[data-class="' + dataAtr + '"]').each(function (i, el) {
//                         $(el).insertAfter(parentElem);
//                     });
//                     if (!group) {
//                         $('.childGroup[data-class="' + dataAtr + '"]').hide()
//                         var ellem = $('.parentGroup[data-classTwoGroup ^=' + parentElementAtr + ']');
//                         $(ellem).find('.twoGroup').removeClass('reActiveTdElem');
//                         $(ellem).find('.twoGroup').addClass('activeTdElem');
//                         $(ellem).hide();
//                     }
//                     else $('.childGroup[data-classTwoGroup="' + dataAtr + '"]').hide();
//                     if (!group && !manufac) check(false)
//                 }
//                 else {
//                     var parentElem = $(this).parent();
//                     $(this).removeClass('activeTdElem');
//                     $(this).addClass('reActiveTdElem');
//                     //css
//                     $(this).parent().addClass('activeGroupOne');
//                     $(this).parent().removeClass('activeGroup');
//                     // выбираем детей
//                     var childs = $('.childGroup[data-class="' + dataAtr + '"]').find('.manufacturerTd');
//
//                     //выбираем td.manufactured главного списка
//                     var childTdParentElem = $(parentElem).find('.manufacturerTd');
//
//                     var childsTr = $('.childGroup[data-class="' + dataAtr + '"]');
//                     //css
//                     $(childsTr).addClass('activeGroupOne');
//                     $('.childGroup[data-class="' + dataAtr + '"]').each(function (i, el) {
//                         $(el).insertAfter(parentElem);
//                     });
//                     if (!group)$('.childGroup[data-class="' + dataAtr + '"]').show();
//                     else {
//                         $('.childGroup[data-classTwoGroup="' + dataAtr + '"]').show();
//
//                     }
//                     if (!group && !manufac) {
//                         groupElements(childs, tBody_eurocode_searchCar, '.manufacturerTd', false, 1, childsTr);
//                         //проверяем совпадения с parentElementTr
//                         check(true);
//                     }
//
//
//                 }
//                 function check(status) {
//                     var parentText = $(childTdParentElem).text();
//                     var first = $(parentElem).attr('data-class');
//                     var flagCheck = false;
//                     var flagOneLine = true;
//                     //определяем data-class ATr для связки
//                     //скрываем
//                     $(childs).each(function (i, elem) {
//
//                         var childText = $(elem).text();
//                         if (parentText == childText) {
//                             $(elem).off();
//                             $(elem).removeClass('reActiveTdElem');
//                             $(elem).removeClass('activeTdElem');
//                             var childTr = $(elem).parent();
//                             //связываем
//                             $(childTr).attr('data-svyzka', first);
//                             $(childTr).addClass('svyzkaChild')
//                             $(parentElem).attr('data-svyzka', first)
//                             $(childTr).hide();
//                             flagCheck = true;
//                         }
//                         else {
//                             flagOneLine = false;
//                         }
//                     })
//                     //добавлем + и вешаем обработчик
//                     if (flagCheck) {
//
//                         if (status) {
//                             $(childTdParentElem).removeClass('reActiveTdElem');
//                             $(childTdParentElem).addClass('activeTdElem');
//                             $(childTdParentElem).on('click', thirdFunc);
//                         }
//                         else {
//                             $(childTdParentElem).off();
//                             $(childTdParentElem).removeClass('activeTdElem');
//                             $(childTdParentElem).removeClass('reActiveTdElem');
//                         }
//
//                     }
//                     //проверяем на 1 линию
//                     (function checkLine() {
//                         if (flagOneLine) $(childTdParentElem).trigger('click', thirdFunc);
//                     })();
//                 }
//
//                 function thirdFunc() {
//                     var parentAtr = $(this).parent().attr('data-svyzka');
//                     if (flagThirdFunc) {
//                         $('tr[data-svyzka=' + parentAtr + ']').each(function (i, el) {
//                             $(el).insertAfter(parentElem);
//                         })
//                         $('tr[data-svyzka=' + parentAtr + ']').show() && (flagThirdFunc = false);
//                         $(this).parent().nextAll('tr[data-svyzka=' + parentAtr + ']').removeClass('activeGroupOne');
//                         $(this).parent().nextAll('tr[data-svyzka=' + parentAtr + ']').addClass('activeGroupTwo');
//                         //     $('tr[data-svyzka='+parentAtr+']').addClass('activeGroupTwo');
//                         $(childTdParentElem).removeClass('activeTdElem');
//                         $(childTdParentElem).addClass('reActiveTdElem');
//
//                     }
//                     else {
//                         $(this).parent().nextAll('tr[data-svyzka=' + parentAtr + ']').addClass('activeGroupOne');
//                         $(this).parent().nextAll('tr[data-svyzka=' + parentAtr + ']').removeClass('activeGroupTwo');
//                         $('.svyzkaChild[data-svyzka=' + parentAtr + ']').hide() && (flagThirdFunc = true);
//                         //   $('tr[data-svyzka='+parentAtr+']').removeClass('activeGroupTwo');
//                         $(childTdParentElem).removeClass('reActiveTdElem');
//                         $(childTdParentElem).addClass('activeTdElem');
//
//                     }
//                 }
//             };
//
//
//         };
//
//
// //функция выборки минимального значения из массива
//         Array.min = function (array) {
//             return Math.min.apply(Math, array);
//         };
//         Array.max = function (array) {
//             return Math.max.apply(Math, array);
//         };
//         /*  ПОИСК ПО ЕВРОКОДУ  */
//         $('#eurocode').find('.btn-default').on('click', getEurocodeData.bind(null, false));
//
//         function getEurocodeData(flag, eurocode, local, auto) {
//             console.log('jhhjhj');
//             if (auto) autoStatus = true
//             else autoStatus = false;
//             console.log('yes');
//             if (!autoStatus) localFilterMas = [];
//             //для автозаполнения фильтров
//             if (localStorage.getItem('filter') != null && autoStatus) {
//                 console.log(localStorage);
//                 var localFilter = JSON.parse(localStorage.getItem('filter'));
//                 $(localFilter).each(function (i, el) {
//                     localFilterMas.push(el);
//                 })
//
//             }
//             var getCity = $('.city').attr('data-cityId');
//             $('.preloader').removeClass('none');
//             if (!flag) {
//                 var inputData;
//                 if (local == undefined) {
//                     localStorage.clear();
//                     localFilterMas = [];
//                     inputData = $(input_eurocode).val()
//                 }
//                 else {
//                     inputData = eurocode;
//                     $('#navTabs > li').removeClass('active');
//                     $('#navTabs > li').eq(1).addClass('active');
//                     $('.tab-pane').removeClass('active', 'in')
//                     $('#searchByCode').addClass('active in');
//                 }
//                 localStorage.setItem('eurocode', inputData);
//                 var http = new XMLHttpRequest();
//                 http.open('GET', '/search/car_by_eurocode/?eurocode=' + inputData);
//                 http.onreadystatechange = function () {
//                     console.log(http.readyState)
//                     if (http.readyState == 4) {
//                         if (http.status == 200) {
//                             var data = JSON.parse(http.responseText);
//
//                             sessionStorage.setItem('car', data[0].auto);
//                             sessionStorage.setItem('itemName', data[0].glasstype);
//                             $('.carName').text(sessionStorage.getItem('car'));
//                             $('.itemName').text(sessionStorage.getItem('itemName'))
//                         }
//
//                     }
//                 };
//                 http.send(null);
//
//             }
//             else {
//                 var inputData = eurocode;
//             }
//             var xmlhttp = new XMLHttpRequest();
//             xmlhttp.open('GET', '/search/eurocode/?eurocode=' + inputData + '&cityId=' + getCity + '', true);
//             xmlhttp.onreadystatechange = function () {
//                 if (xmlhttp.readyState == 4) {
//                     if (xmlhttp.status == 200) {
//                         var data = xmlhttp.responseText;
//
//                         (!flag) ? render(data, false, inputData) : render(data, true, inputData);//ForSearch car;
//                         (!flag) ? $(table_eurocode).trigger('update') : $(table_eurocode_searchCar).trigger('update');
//                         $('.preloader').addClass('none');
//                         //Группировка
//                         //default по eurocode
//                         if (!flag) {
//                             $(tBody_eurocode).find('tr:visible').addClass('viewElems');
//                             groupElements($('.viewElems').find('.eurocodeTd'), tBody_eurocode, '.eurocodeTd')
//                             var event = new Event('click');
//                             document.getElementById('tdEurocodeHeadTr2').dispatchEvent(event);
//
//                             //вешаем обработчики событий для кнопок сортировки по еврокоду производителю дистрибьютеру
//                             $(table_eurocode).find('.tablesorter-headerRow').find('td').eq(1).click(function () {
//                                 $(table_eurocode).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.eurocodeTd'), tBody_eurocode, '.eurocodeTd');
//                             })
//                             $(table_eurocode).find('.tablesorter-headerRow').find('td').eq(2).click(function () {
//                                 $(table_eurocode).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.manufacturerTd'), tBody_eurocode, '.manufacturerTd', true)
//                             })
//
//                             $(table_eurocode).find('.tablesorter-headerRow').find('td').eq(3).click(function () {
//                                 $(table_eurocode).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.dillerTd'), tBody_eurocode, '.dillerTd');
//                             })
//
//                             $(table_eurocode).find('.tablesorter-headerRow').find('td').eq(4).click(function () {
//                                 $(table_eurocode).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.ycTd'), tBody_eurocode, '.ycTd');
//                             })
//                         }
//                         else {
//
//                             $(tBody_eurocode_searchCar).find('tr:visible').addClass('viewElems');
//                             groupElements($('.viewElems').find('.eurocodeTd'), tBody_eurocode_searchCar, '.eurocodeTd');
//                             var event = new Event('click');
//                             document.getElementById('tdEurocodeHead').dispatchEvent(event);
//
//                             //вешаем обработчики событий для кнопок сортировки по еврокоду производителю дистрибьютеру
//                             $(table_eurocode_searchCar).find('.tablesorter-headerRow').find('td').eq(1).click(function () {
//                                 $(table_eurocode_searchCar).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode_searchCar).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.eurocodeTd'), tBody_eurocode_searchCar, '.eurocodeTd');
//                             })
//                             $(table_eurocode_searchCar).find('.tablesorter-headerRow').find('td').eq(2).click(function () {
//                                 $(table_eurocode_searchCar).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode_searchCar).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.manufacturerTd'), tBody_eurocode_searchCar, '.manufacturerTd', true)
//                             })
//
//                             $(table_eurocode_searchCar).find('.tablesorter-headerRow').find('td').eq(3).click(function () {
//                                 $(table_eurocode_searchCar).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode_searchCar).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.dillerTd'), tBody_eurocode_searchCar, '.dillerTd');
//                             })
//
//                             $(table_eurocode_searchCar).find('.tablesorter-headerRow').find('td').eq(4).click(function () {
//                                 $(table_eurocode_searchCar).find('td').removeClass('activeTdElem');
//                                 $(table_eurocode_searchCar).find('td').removeClass('reActiveTdElem');
//                                 groupElements($('.viewElems').find('.ycTd'), tBody_eurocode_searchCar, '.ycTd');
//                             })
//
//                         }
//
//
//                     }
//                 }
//             };
//             xmlhttp.send(null);
//
//             /*
//              @parametres : d {objet} - JSON data, flag {boolean} - флаг для рендеринга таблицы, code {string} - еврокод
//              Описание: Рэндерит таблицу с результатами запросов как для страницы ПОИСК АВТОМОБИЛЕЙ  так и для ЕВРОКОД, В зависимсоти от переданоого параметра FLAG */
//
// ;
//             return false;
//         };
//
//
//         $('.itemName').text(sessionStorage.getItem('itemName'));
//         $('.carName').text(sessionStorage.getItem('car'));
// })
// })(jQuery);