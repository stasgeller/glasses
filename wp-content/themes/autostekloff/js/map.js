(function ($) {
    var data_json;
    $(document).on('ready', function () {
        //console.log(php_vars.url);
        $.ajax({
            url: php_vars.url,
            type: 'post',
            data: '&action=get_json',
            dataType: 'json',

            success: function (data) {

                function initialize() {
                    var latlng = new google.maps.LatLng(55.75222, 37.61556);
                    var myOptions = {
                        zoom: 10,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("map_place"),
                        myOptions);

                    for (var i = 0; i < data.json.length; i++) {

                        var myLatLng = {
                            lat: parseFloat(data.json[i].install_point_latitude),
                            lng: parseFloat(data.json[i].install_point_longitude)
                        };

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: data.json[i].install_center_name
                        });
                    }


                }

                google.maps.event.addDomListener(window, "load", initialize);
            }

        });
    });
})(jQuery);