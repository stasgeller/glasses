(function($) {

    $(document).ready(function() {

        $('.fancy').fancybox({});

        var si = $('.scroller').find('.item').size();
        var sw = $('.scroller').find('.item').width();

        $('.scroller').width(si * sw + 5).scroller();

        $(document).on('resize', function () {
            var si = $('.scroller').find('.item').size();
            var sw = $('.scroller').find('.item').width();
            $('.scroller').width(si * sw + 5).scroller();
        });

        $('.orders').find('.item.open').each(function () {
            $(this).find('.body').show();
        });

        $('.orders').find('.dock').on('click', function () {
            $(this).closest('.item').toggleClass('open');
            if ($(this).closest('.item').hasClass('open')) {
                $(this).next('.body').show('slide', {'direction': 'up'}, 300);
            } else {
                $(this).next('.body').hide('slide', {'direction': 'up'}, 300);
            }
        });

        $('.form-styler').styler();

        $('#time').mask('99:99');

        $('#phone').mask('+7 (999) 999 99 99');

        $('.city').click(function () {
            $(this).next('.float').toggle('fade', 300);
            return false;
        });

        $('body').click(function () {
            $('.float').hide('fade', 300);
            $('.float').on('click', function (e) {
                e.stopPropagation();
            })
        });

        $('.orders .items').on('click', 'button', function () {
            $('.orders').find('.popup').hide('fade', 300);
            $(this).closest('.items').find('.popup').show('fade', 300);
            return false;
        });

        $('.orders .popup').on('click', '.close', function () {
            $(this).closest('.popup').hide('fade', 300);
            return false;
        });

        $('.choose_center').on('click', 'a.show', function () {
            $(this).closest('.item').toggleClass('open');
            return false
        });

        // $('.choose_glass').on('click', '.clickable', function(event) {
        //     var target = event.target;
        //     if(target.tagName != 'BUTTON' || !$(target).hasClass('compare')) {
        //         $(this).closest('.item').toggleClass('open');
        //     }
        //     $(this).find('a.show').click(function(e) {
        //         e.preventDefault();
        //     });
        //     console.log();
        // });

        $('.choose_center').on('click', '.clickable', function (event) {
            var target1 = event.target;
            if (target1.tagName != 'BUTTON' || target1.tagName != 'A') {
                $(this).closest('.item').toggleClass('open');
            }
            $(this).find('a.show').click(function (e) {
                e.preventDefault();
            });
            console.log(target1.tagName);
        });

        $('.radioblock').on('click', '.text a', function () {
            $(this).toggleClass('show');
            $(this).prev('p').toggleClass('show');
            return false;
        });

        $('.stick').on('click', '.active', function () {
            if (!$(this).closest('.item').hasClass('open')) {
                $(this).closest('.item').toggleClass('open');
            }
        });

        $('.stick').on('click', '.title a', function () {
            $(this).closest('.item').toggleClass('open');
            return false
        });

        $('.sn').click(function () {
            $('menu').show('slide', {'direction': 'left'}, 300);
            return false;
        });

        $('header, main, footer').click(function () {
            $('menu').hide('slide', {'direction': 'left'}, 300);
        });

        $('.link-select').on('click', 'a.select', function () {
            $('a.select').removeClass('show');
            $(this).addClass('show');
            $('.s_popup').hide('fade', 150);
            $(this).next('.s_popup').show('fade', 150);
            return false;
        });
        $('body').click(function (e) {
            var target2 = event.target;
            if (!$(target2).hasClass('link-select')) {
                $('a.select').removeClass('show');
                $('.s_popup').hide('fade', 150);
            }
        });

        $('.btn-close-login-modal').on('click', function () {
            $('.modal-login').modal('hide')
        });

        $('.choose_glass .item .title strong').on('click', function () {
            $(this).parents('.item').toggleClass('open')
            return false
        });

        $('.choose_glass .item .show').on('click', function () {
            $(this).parents('.item').toggleClass('open')
            return false
        });

        var map = $('.button_page_three ul li:nth-child(2)');
        $('#gmap').addClass('noactive_gmap');


        $('.button_page_three ul li').on('click', function(e){
            e.preventDefault();
            $('.button_page_three ul li').removeClass('active');
            $(this).addClass('active');


            if($(map).hasClass('active')){
                $('#gmap').removeClass('noactive_gmap');
                $('.result_centers, .sorter_dock').addClass('noactive_bg');
            } else {
                $('#gmap').addClass('noactive_gmap');
                $('.result_centers, .sorter_dock').removeClass('noactive_bg');
            }
        });


    });



})(jQuery);
