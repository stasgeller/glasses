<?php global $autostekloff; ?>   
<footer>
    <script>
		var onloadCallback = function() {
			mysitekey = '6LfG6SMTAAAAAD0zfC0Yw9_DvR6xqrW1y5uQkvO_';
			grecaptcha.render('captcha1', {
				'sitekey' : mysitekey
			});
			grecaptcha.render('captcha2', {
				'sitekey' : mysitekey
			});
		};
  </script>
      <div class="container">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="row">
            <a href="#" class="footer-logo"><img src="<?php echo $autostekloff['footer-logo']['url']?>" alt=""></a>
            <div class="hidden-xs footer-copy"><?php echo $autostekloff['footer-copyright'] ?></div>
          </div>
        </div>
        <div class="hidden-xs col-sm-3 col-md-3 col-lg-3">
          <div class="row">
            <div class="footer-title"><?php _e( 'Меню', 'autostekloff' )?></div>
            <?php wp_nav_menu(array('theme_location' => 'footer-menu', 'container' => '','menu_class'=>'footer-menu' )); ?>
          </div>
        </div>
        <div class="hidden-xs col-sm-3 col-md-3 col-lg-3">
          <div class="row">
            <div class="footer-title"><?php _e( 'Информация', 'autostekloff' )?></div>
            <?php wp_nav_menu(array('theme_location' => 'info-menu', 'container' => '','menu_class'=>'footer-menu' )); ?>            
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="row">
            <div class="footer-title hidden-xs"><?php _e( 'Контакты', 'autostekloff' )?></div>
            <div class="footer-contacts hidden-xs">
              <div class="footer-contacts-item"><i class="icons-f-phone"></i><?php echo $autostekloff['footer-tel'] ?></div>
              <div class="footer-contacts-item"><i class="icons-f-address"></i><?php echo $autostekloff['footer-address'] ?></div>
              <div class="footer-contacts-item"><i class="icons-f-mail"></i><u><?php echo $autostekloff['footer-email'] ?></u></div>
            </div>
            <div class="footer-socio">
              <?php _e( 'Будьте на связи:', 'autostekloff' )?>
              <div>
                <a href="<?php echo $autostekloff['footer-vk'] ?>" class="footer-socio-vk"></a>
                <a href="<?php echo $autostekloff['footer-fb'] ?>" class="footer-socio-fb"></a>
                <a href="<?php echo $autostekloff['footer-tw'] ?>" class="footer-socio-tw"></a>
              </div>
            </div>
          </div>
        </div>
        <div class="cleafix"></div>
      </div>
    </footer>
    <div class="modal fade" id="questModal" tabindex="-1" role="dialog" aria-hidden="true" class="bs-example-modal-sm">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" id="myModalLabel"><?php _e( 'Задать вопрос', 'autostekloff' )?></h4>
          </div>
          <div class="modal-body">
            <form id="frm-send-question" action="">
              <div class="form-group">
                <div><input type="text" class="form-control" placeholder="<?php _e( 'Как вас зовут?', 'autostekloff' )?>" name="name"></div>
              </div>
              <div class="form-group">
                <div><input id="send-question-mail" type="text" class="form-control" placeholder="<?php _e( 'Ваш e-mail', 'autostekloff' )?>" name="mail"><!-- <div id="valid" class="mail-error"></div> --></div>
              </div>
              <div class="form-group">
                <div><textarea class="form-control" placeholder="<?php _e( 'Текст вопроса:', 'autostekloff' )?>" name="text"></textarea></div>
              </div>
			  <div class="form-group">
                <div>
				  <div id="captcha1"></div>
                </div>
			  </div>
			  <!-- <div class="modal-footer"> -->
			  <div class="modal-footer">
				<div>
					<button type="button" class="btn btn-primary" id="send-question"><?php _e( 'Отправить вопрос', 'autostekloff' )?></button>
				</div>	
			  </div>
			  <!-- </div> -->
              <!-- <div class="form-group col-xs-12 col-sm-7 col-lg-7">
                <div>
                  <input type="text" class="form-control" placeholder="<?php _e( 'Ответ', 'autostekloff' )?>" name="calc">
                </div>
              </div> -->
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="serviseModal" tabindex="-1" role="dialog" aria-hidden="true" class="bs-example-modal-sm">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title" id="myModalLabelnew"><?php _e( 'Заполните заявку и сэкономьте до 70% при замене стекла', 'autostekloff' )?></h4>
          </div>
          <div class="modal-body">
            <form id="form-new" name="form-new">
                <div class="form-group">
                    <div>
                        <input id="name-formnew" class="form-control required" type="text" name="name" placeholder="<?php _e( 'Введите имя', 'autostekloff' )?>">
                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <input id="phone-formnew" class="form-control required" type="text" name="phone" placeholder="<?php _e( 'Введите телефон', 'autostekloff' )?>">
                    </div>
                </div>
                <div class="form-group">
                    <div>
                        <input id="brand-formnew" class="form-control" type="text" name="brand" placeholder="<?php _e( 'Марка / модель / год выпуска', 'autostekloff' )?>">
                    </div>
                </div>
                <div class="form-group form-check">
                    <div>
                        <input id="accept-new" class="form-control" type="checkbox">
                        <label for="accept">
                            <span><?php _e( 'Я согласен единоразово получить информацию по автостеклам', 'autostekloff' )?></span>
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 box-btn">
                    <div class="row">
                        <input id="btn-send-formnew" class="btn btn-primary" type="submit" title="<?php _e( 'найти стекло', 'autostekloff' )?>" value="<?php _e( 'найти стекло', 'autostekloff' )?>">
                    </div>
                </div>
                <div class="hidden-xs col-sm-12 col-md-12 col-lg-12 box-btn">
                    <div class="row">
                        <em><?php _e( 'Это быстро и бесплатно :-)', 'autostekloff' )?></em>
                    </div>
                </div>
                <div class="clearfix"></div>
            </form>
          </div>          
        </div>
      </div>
    </div>
    <div id="callbackModal" class="modal fade bs-example-modal-sm" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                    <h4 id="myModalLabelcallback" class="modal-title">
                    <?php _e( 'Заказать', 'autostekloff' )?>
                    <br>
                    <?php _e( 'обратный звонок', 'autostekloff' )?>
                    </h4>
                </div>
                <div class="modal-body">
                    <form id="form-callback" name="form-callback">
                        <div class="form-group">
                            <div>
                                <input id="name-callback" class="form-control required" type="text" name="name-callback" placeholder="<?php _e( 'Как вас зовут?', 'autostekloff' )?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <input id="phone-callback" class="form-control required" type="text" name="phone-callback" placeholder="<?php _e( 'Номер телефона', 'autostekloff' )?>">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="btn-request-call-me" class="btn btn-primary" type="button"><?php _e( 'Перезвоните мне', 'autostekloff' )?></button>
                </div>
            </div>
        </div>
    </div>
    <div id="thanksModal" class="modal fade bs-example-modal-sm" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                    <h4 id="myModalLabelthanks" class="modal-title"><?php _e( 'Спасибо!', 'autostekloff' )?></h4>
                </div>
                <div class="modal-body"><?php _e( 'Наши сотрудники свяжутся с Вами в самое ближайшее время!', 'autostekloff' )?>  </div>
                <div class="modal-footer">
                    <button id="btn-thanks-close" class="btn btn-primary" type="button"><?php _e( 'Жду звонка!', 'autostekloff' )?></button>
                </div>
            </div>
        </div>
    </div>
	<div id="thanksQuestionModal" class="modal fade bs-example-modal-sm" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                    <h4 id="myModalLabelthanks" class="modal-title"><?php _e( 'Спасибо!', 'autostekloff' )?></h4>
                </div>
                <div class="modal-body"><?php _e( 'Наши сотрудники дадут ответы на Ваши вопросы в самое ближайшее время!', 'autostekloff' )?>  </div>
                <div class="modal-footer">
                    <button id="btn-thanks-question-close" class="btn btn-primary" type="button"><?php _e( 'Жду ответа!', 'autostekloff' )?></button>
                </div>
            </div>
        </div>
    </div>
	<div id="mailErrorModal" class="modal fade bs-example-modal-sm" aria-hidden="true" role="dialog" tabindex="-1" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                    <h4 id="myModalLabelmailerror" class="modal-title"><?php _e( 'Ошибка!', 'autostekloff' )?></h4>
                </div>
                <div class="modal-body"><?php _e( 'Вы указали неверный e-mail!', 'autostekloff' )?>  </div>
                <div class="modal-footer">
                    <button id="btn-mailerror-close" class="btn btn-primary" type="button"><?php _e( 'Ok', 'autostekloff' )?></button>
                </div>
            </div>
        </div>
    </div>
  <?php wp_footer(); ?>
  
	<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
          ga('create', 'UA-51519364-1', 'auto');
          ga(function(tracker) {
         var clientId = tracker.get('clientId'); // get client id from  Google Analytics
         document.cookie = "_ga_cid=" + clientId + "; path=/"; // save it to cookie _ga_cid
		 document.cookie = "cid=" + clientId + "; path=/"; // save it to cookie cid
         ga('set', 'dimension1', clientId); //запись clientId для каждого пользователя
        });
         ga('send', 'pageview');
	</script>
	<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter25138733 = new Ya.Metrika({id:25138733,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                } catch(e) { }
            });
            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
        
            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/25138733" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <!-- ---------------------------------------------------------------------------- -->
    <!-- Код тега ремаркетинга Google -->
    <!-- С помощью тега ремаркетинга запрещается собирать информацию, по которой можно
         идентифицировать личность пользователя. Также запрещается размещать тег на страницах
         с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега
         читайте на странице http://google.com/ads/remarketingsetup.
    -->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 965608448;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

    <noscript>
        <div style="display:inline;">
            <img
                height="1" width="1" style="border-style:none;" alt=""
                src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/965608448/?value=0&amp;guid=ON&amp;script=0"
            />
        </div>
    </noscript>
	
	<script type='text/javascript'>
    (function(){
        window.ct_cid = '200';
        window.ct_url = '//track.sipuni.com/echo';
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
        s.src = '//track.sipuni.com/static/calltracking/js/client.js'; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}
    )();
    </script>
    <!-- ---------------------------------------------------------------------------- -->
  </body>
</html>

