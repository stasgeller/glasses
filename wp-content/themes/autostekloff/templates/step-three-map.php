<?php
/**
 * Template Name: step-three-map
 *
 */
get_header();
//echo '<pre>'; print_r($json_markers); echo '</pre>';
?>
<main>
    <div class="container">
        <div class="row breadcrumb">
            <ul class="col-sm-12">
                <li><a href="#">Главная</a></li>
                <li><span>Подбор стекла</span></li>
            </ul>
            <h1 class="col-sm-12">ПОДБОР СТЕКЛА В АВТОСТЕКЛОФФ</h1>
        </div>
        <div class="row title">
            <div class="graphic step3">
                <div class="col-sm-4"><a href="#"></a></div>
                <div class="col-sm-4"><a href="#"></a></div>
                <div class="col-sm-4"><a href="#"></a></div>
            </div>
        </div>
    </div>
    <div class="bg">
        <div class="container">
            <div class="row content choose_center map">
                <h3>давайте вместе выберем установочный центр</h3>
                <div class="view_nav">
                    <ul>
                        <li>
                            <a href="255-2/shag-2__trashed/shag-3‎">
                                <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_list.png" alt="">
                                <span>Список</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="http://autostekloff.ru/255-2/shag-2/shag-3-karta/">
                                <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_map.png" alt="">
                                <span>Карта</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="gmap">
        <div class="view_nav left_view_nav">
            <ul>
                <li>
                    <a href="">
                        <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_list.png" alt="">
                        <span>Все</span>
                    </a>
                </li>
                <li class="active">
                    <a href="">
                        <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_map.png" alt="">
                        <span>Избранное</span>
                    </a>
                </li>
            </ul>
        </div>
        <div id="map_place">

        </div>

        <div class="legend">
            <ul>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/img/marker.png" alt="">
                    <p>Установочные центры</p>
                </li>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/img/marker-s.png" alt="">
                    <p>Избранное</p>
                </li>
            </ul>
        </div>

    </div>

    <div class="bg">
        <div class="container">
            <div class="row content choose_center bmap">
                <div class="butt">
                    <p class="title">Выбрали? Тогда мы можем перейти к следующему шагу</p>
                    <button class="btn-default">выбрать еще стекло</button>
                    <p>или</p>
                    <button class="btn-primary">Оформить заказ <i></i></button>
                </div>
                <span>Возникли вопросы?<br/>Звоните: +7 (499) 686-08-45</span>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
