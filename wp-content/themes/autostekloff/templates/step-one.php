<?php
session_start();
/**
 * Template Name: step-one
 *
 */
?>
<?php if (isset($_GET['years']) && $_GET['years'] != "" && isset($_GET['modification']) && $_GET['modification'] != "" && isset($_GET['model']) && $_GET['model'] != "" && isset($_GET['mark']) && $_GET['mark'] != "") {
    $modification = $_GET['modification'];
    $mark = $_GET['mark'];
    $model_auto = $_GET['model'];
    $years = $_GET['years'];
$json_e = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/cars/?mark=' . $mark . '&model=' . $model_auto . '&years=' . $years);
$json_e = json_decode($json_e, true);

?>
<?php
    foreach ($json_e as $modification2) {
         if($modification2['auto'] == $modification){
            $eurocode = $modification2['eurocode4char'];

    }
}

    $cook_val = array('mark' => $mark, 'model' => $model_auto, 'years' => $years, 'modification' => $modification , 'eurocode' => $eurocode);

    setcookie('info_glass', serialize($cook_val),time()+time(),"/");

    $issetget = 1;
}


get_header();
?>
<main>
    <div class="container">
        <div class="row breadcrumb">
            <ul class="col-sm-12">
                <li><a href="#">Главная</a></li>
                <li><span>Подбор стекла</span></li>
            </ul>
            <h1 class="col-sm-12">ПОДБОР СТЕКЛА В АВТОСТЕКЛОФФ</h1>
        </div>
        <div class="row title">
            <div class="graphic step1">
                <div class="col-sm-4"><a href="#"></a></div>
                <div class="col-sm-4"><a href="<?php if ($issetget == 1) {
                        echo "http://autostekloff.ru/step-two/";
                    } else {
                        echo "#scroll";
                    } ?>"></a></div>
                <div class="col-sm-4"><a href="<?php if ($issetget == 1) {
                        echo "http://autostekloff.ru/step-two/";
                    } else {
                        echo "#scroll";
                    } ?>"></a></div>
            </div>
        </div>
    </div>
    <div class="bg">
        <div class="container">
            <div id="scroll" class="row content choose_glass">
                <h3>давайте определим, какой у вас АВТОМОБИЛь</h3>
                <form class="link-select">
                    <fieldset class="col-md-3 col-sm-6 col-xs-12">
                        <a href="" class="select"><?php
                            if (isset($_GET['mark']) && $_GET['mark'] != "") {
                                echo $_GET['mark'];
                            } else {
                                echo "Выберите марку";
                            } ?></a>
                        <div class="s_popup">
                            <?php
                            $json = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/mark/');
                            $json = json_decode($json, true);
                            ?>
                            <?php foreach ($json as $mark) { ?>
                                <?php foreach ($mark as $mark_auto) { ?>
                                    <div class="col-md-2 col-sm-4 col-xs-12">
                                        <a href="http://autostekloff.ru/step-one/?mark=<?php echo $mark_auto ?>#scroll"><?php echo $mark_auto ?></a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </fieldset>
                    <?php
                    if (isset($_GET['mark']) && $_GET['mark'] != "") {
                        $mark = $_GET['mark'];
                        $json2 = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/model/?mark=' . $mark);
                        $json2 = json_decode($json2, true);
                    }
                    ?>
                    <fieldset class="col-md-3 col-sm-6 col-xs-12">

                        <a href="" class="select"><?php if (isset($_GET['model']) && $_GET['model'] != "") {
                                echo $_GET['model'];
                            } else {
                                echo "Выберите модель";
                            } ?></a>
                        <div <?php if (!isset($_GET['mark'])) {
                            echo "style='display:none'>";
                        } else {
                            echo "class=\"s_popup\">";
                        } ?>
                        <?php foreach ($json2 as $model) { ?>
                            <?php foreach ($model as $model_auto) { ?>
                                <div class="col-md-2 col-sm-4 col-xs-12"><a
                                        href="http://autostekloff.ru/step-one/?mark=<?php echo $mark ?>&model=<?php echo $model_auto ?>#scroll"><?php echo $model_auto ?></a>
                                </div>
                            <?php } ?>
                        <?php } ?>
            </div>
            </fieldset>
            <?php
            if (isset($_GET['model']) && $_GET['model'] != "") {
                $model_auto = $_GET['model'];
                $json3 = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/years/?mark=' . $mark . '&model=' . $model_auto);
                $json3 = json_decode($json3, true);
            }
            ?>
            <fieldset class="col-md-3 col-sm-6 col-xs-12">
                <a href="" class="select"><?php if (isset($_GET['years']) && $_GET['years'] != "") {
                        echo $_GET['years'];
                    } else {
                        echo "Выберите годы выпуска";
                    } ?></a>
                <div <?php if (!isset($_GET['model'])) {
                    echo "style='display:none'>";
                } else {
                    echo "class=\"s_popup\">";
                } ?>
                <?php foreach ($json3 as $years) { ?>
                    <?php foreach ($years as $years_auto) { ?>
                        <div class="col-md-2 col-sm-4 col-xs-12"><a
                                href="http://autostekloff.ru/step-one/?mark=<?php echo $mark ?>&model=<?php echo $model_auto ?>&years=<?php echo $years_auto ?>#scroll"><?php echo $years_auto ?></a>
                        </div>
                    <?php } ?>
                <?php } ?>
        </div>
        </fieldset>
        <?php
        if (isset($_GET['years']) && $_GET['years'] != "") {
            $years = $_GET['years'];
            $json4 = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/cars/?mark=' . $mark . '&model=' . $model_auto . '&years=' . $years);
            $json4 = json_decode($json4, true);
        }
        ?>
        <fieldset class="col-md-3 col-sm-6 col-xs-12">
            <a href="" class="select"><?php if (isset($_GET['modification']) && $_GET['modification'] != "") {
                    echo $_GET['modification'];
                } else {
                    echo "Выберите тип кузова";
                } ?></a>
            <div <?php if (!isset($_GET['years'])) {
                echo "style='display:none'>";
            } else {
                echo "class=\"s_popup\">";
            } ?>
            <?php foreach ($json4 as $modification) { ?>
                <?php foreach ($modification as $key => $modification_auto) { ?>
                    <?php if ($key == 'auto') { ?>
                        <div class="col-md-2 col-sm-4 col-xs-12"><a
                                href="http://autostekloff.ru/step-one/?mark=<?php echo $mark ?>&model=<?php echo $model_auto ?>&years=<?php echo $years ?>&modification=<?php echo $modification_auto ?>#scroll"><?php echo $modification_auto; ?></a>
                        </div>
                    <?php }
                } ?>
            <?php } ?>
    </div>
    </fieldset>


    <strong>Фото</strong>
    <a href="<?php bloginfo('template_directory'); ?>/img/car.png" rel="auto" class="fancy col-sm-3"><img
            src="<?php bloginfo('template_directory'); ?>/img/car.png" alt=""></a>
    <a href="<?php bloginfo('template_directory'); ?>/img/car.png" rel="auto" class="fancy col-sm-3"><img
            src="<?php bloginfo('template_directory'); ?>/img/car.png" alt=""></a>
    <a href="<?php bloginfo('template_directory'); ?>/img/car.png" rel="auto" class="fancy col-sm-3"><img
            src="<?php bloginfo('template_directory'); ?>/img/car.png" alt=""></a>
    <a href="<?php bloginfo('template_directory'); ?>/img/car.png" rel="auto" class="fancy col-sm-3"><img
            src="<?php bloginfo('template_directory'); ?>/img/car.png" alt=""></a>
    <p>Это он? Тогда мы можем перейти к следующему шагу</p>
    <div class="butt">
        <a href="<?php if ($issetget == 1) {
            echo "http://autostekloff.ru/step-two/";
        } else {
            echo "#";
        } ?>" class="btn-primary">Параметры стекла <i></i></a>
    </div>
    <span>Возникли вопросы?<br/>Звоните: +7 (499) 686-08-45</span>
    </form>
    </div>
    </div>
    </div>
</main>

<?php get_footer(); ?>
