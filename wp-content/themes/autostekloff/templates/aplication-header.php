<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="true">
    <meta http-equiv="cleartype" content="on">
    <meta http-equiv="msthemecompatible" content="no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <title>Автостеклофф | ПОДБОР СТЕКЛА</title>

    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap-theme.css">
    <link rel="stylesheet" href="vendors/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="vendors/jquery-ui/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="vendors/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="vendors/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="vendors/jquery.fs.scroller/jquery.fs.scroller.css">
    <link rel="stylesheet" href="vendors/jquery-form-styler/jquery.formstyler.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/media.css">

    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/jquery-ui/jquery-ui.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/fancybox/source/jquery.fancybox.js"></script>
    <script src="vendors/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script src="vendors/jquery.fs.scroller/jquery.fs.scroller.js"></script>
    <script src="vendors/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
    <script src="vendors/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="vendors/slick-carousel/slick/slick.min.js"></script>
    <script src="vendors/jquery.tablesorter.min.js"></script>
    <script src="vendors/jquery-form-styler/jquery.formstyler.min.js"></script>
    <script src="assets/js/main.js"></script>

</head>

<body>
<header class="hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 nav">
                <ul>
                    <li><a href="#">Подобрать стекло</a></li>
                    <li><a href="#">Автосервисы</a></li>
                    <li><a href="#">Компания</a></li>
                    <li><a href="#">Партнерам</a></li>
                    <li><a href="#">Блог</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 col-sm-8">
                <div class="row">
                    <div class="col-sm-4 logo">
                        <a href="#"><img src="assets/i/logo.png" alt=""></a>
                    </div>
                    <div class="col-sm-4 addr">
                        <span>Ваш город:</span><a href="#" class="city">Москва</a>
                        <div class="float">
                            <ul>
                                <li><a href="#">Москва</a></li>
                                <li><a href="#">Краснодар</a></li>
                                <li><a href="#">Тверь</a></li>
                                <li><a href="#">Самара</a></li>
                                <li><a href="#">Пенза</a></li>
                                <li><a href="#">Красноуфимск</a></li>
                                <li><a href="#">Выборг</a></li>
                                <li><a href="#">Владивосток</a></li>
                                <li><a href="#">Керчь</a></li>
                                <li><a href="#">Пермь</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 phone">
                        <p>+7 (499) 686-08-45</p>
                        <a href="#">Задать вопрос</a>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-4">
                <div class="row">
                    <div class="col-sm-6 col-md-3 login">
                        <a href="#" data-toggle="modal" data-target="#enter">Личный кабинет</a>
                    </div>
                    <div class="col-sm-6 col-md-9 exts">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="row">
                                    <div class="col-md-4 ext">
                                        <div class="row"><a href=""><img src="assets/i/ic/ic_nav5.png" alt=""><span>0</span></a></div>
                                    </div>
                                    <div class="col-md-4 ext">
                                        <div class="row"><a href=""><img src="assets/i/ic/ic_nav4.png" alt=""><span>0</span></a></div>
                                    </div>
                                    <div class="col-md-4 ext">
                                        <div class="row"><a href=""><img src="assets/i/ic/ic_nav2.png" alt=""><span>1</span></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-12 ext cart">
                                <div class="row"><a href=""><img src="assets/i/ic/ic_nav3.png" alt=""><span>3</span></a>
                                    <p><a href="#">Корзина<span>12 750 Р</span></a></p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<header class="mobile hidden-sm hidden-md hidden-lg">
    <div class="container">
        <div class="row">
            <div class="nav"><a href="#" class="sn"><i class="glyphicon glyphicon-align-justify"> </i></a></div>
            <div class="logo">
                <img src="assets/i/logo.png" alt="">
            </div>
        </div>
    </div>
</header>

<menu class="mobile hidden-sm hidden-md hidden-lg">
    <div class="addr">
        <span>Ваш город:</span>
        <select>
            <option>Москва
            <option>Краснодар
            <option>Тверь
            <option>Самара
            <option>Пенза
            <option>Красноуфимск
            <option>Выборг
            <option>Владивосток
            <option>Керчь
            <option>Пермь
        </select>
    </div>
    <div class="phone">
        <p>+7 (499) 686-08-45</p>
        <a href="#">Задать вопрос</a>
        <a href="#" data-toggle="modal" data-target="#enter">Личный кабинет</a>
    </div>
    <div class="lk">
        <div class="ext">
            <a href=""><img src="assets/i/ic/ic_nav1.png" alt=""><span>0</span></a>
        </div>
        <div class="ext">
            <a href=""><img src="assets/i/ic/ic_nav2.png" alt=""><span>1</span></a>
        </div>
        <div class="ext cart">
            <a href=""><img src="assets/i/ic/ic_nav3.png" alt=""><span>3</span></a>
            <p><a href="#">Корзина<span>12 750 Р</span></a></p>
        </div>
    </div>
    <div class="nav-nav">
        <ul>
            <li><a href="#">Подобрать стекло</a></li>
            <li><a href="#">Автосервисы</a></li>
            <li><a href="#">Компания</a></li>
            <li><a href="#">Партнерам</a></li>
            <li><a href="#">Блог</a></li>
            <li><a href="#">Контакты</a></li>
        </ul>
    </div>
</menu>
