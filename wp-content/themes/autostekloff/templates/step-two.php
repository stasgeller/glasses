<?php
/**
 * Template Name: step-two
 *
 */
get_header();
$coo_test = stripslashes($_COOKIE['info_glass']);
$cookies = unserialize($coo_test);
$mark = $cookies['mark'];
$model = $cookies['model'];
$years = $cookies['years'];
$eurocode = $cookies['eurocode'];
$modification = $cookies['modification'];
?>

<main>
    <div class="container">
        <div class="row breadcrumb">
            <ul class="col-sm-12">
                <li><a href="#">Главная</a></li>
                <li><span>Подбор стекла</span></li>
            </ul>
            <h1 class="col-sm-12">ПОДБОР СТЕКЛА В АВТОСТЕКЛОФФ</h1>
        </div>
        <div class="row title">
            <div class="graphic step2">
                <div class="col-sm-4"><a href="http://autostekloff.ru/step-one/?mark=<?php echo $mark ?>&model=<?php echo $model ?>&years=<?php echo $years ?>&modification=<?php echo $modification ?>"></a></div>
                <div class="col-sm-4"><a href="#"></a></div>
                <div class="col-sm-4"><a href="#"></a></div>
            </div>
        </div>
    </div>
    <div class="bg">
        <div class="container">
            <div class="row content choose_glass">
                <h3>Давайте вместе с Вами поймем, какое вам нужно стекло</h3>

                <form  method="get" id="formSearchCar">
                    <select id="choose_glass" name="glasstype" id="select1" class="form-styler centred col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3" ">
                    <option disabled selected>Выберите нужное стекло</option>

                        <?php
                        $json = file_get_contents('http://test.avtosteklov.ru/shop/search/cars/glasstype/?ec4='.$eurocode);
                        $json = json_decode($json, true);
                        ?>
                        <?php foreach($json as $glass): ?>

                            <option value="<?= $glass['eurocode']?>">
                                <?php echo $glass['glasstype']?>
                            </option>
                        <?php endforeach;?>

                    </select>
                    <div class="filterparam">

                    <h4 class="col-sm-12">Параметры стекла для вашего <?php  echo  $mark.' '.$model;?></h4>
                    <div class="checkblock clearfix">
                        <div class="col-md-3 col-sm-6">
                            <div class="row">
                                <label for="q01" class="col-xs-12 col-md-12 col-sm-12">

                                    <input type="checkbox" name="color" id="q01" >

                                    <p>цветное стекло</p>
                                </label>
                                <label for="q011" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="color-green" id="q011">
                                    <p>зеленое</p>
                                </label>
                                <label for="q012" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="color-yellow" id="q012">
                                    <p>желтое</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q02" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="solarcontrol" id="q02">
                                    <p>solar control</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q03" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="solid" id="q03">
                                    <p>светозащитная полоса</p>
                                </label>
                                <label for="q031" class="col-xs-12 col-sm-4">
                                    <input type="radio" name="solidblue" id="q031">
                                    <p>синяя</p>
                                </label>
                                <label for="q032" class="col-xs-12 col-sm-4">
                                    <input type="radio" name="solidgrey" id="q032">
                                    <p>серая</p>
                                </label>
                                <label for="q033" class="col-xs-12 col-sm-4">
                                    <input type="radio" name="solidgreen" id="q033">
                                    <p>зеленая</p>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="row">
                                <label for="q04" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q04">
                                    <p>датчик дождя</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q05" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q05">
                                    <p>датчик света</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q06" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q06">
                                    <p>обогрев</p>
                                </label>
                                <label for="q061" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="" id="q061">
                                    <p>всего стекла</p>
                                </label>
                                <label for="q062" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="" id="q062">
                                    <p>зоны щеток</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q07" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q07">
                                    <p>атермальное</p>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="row">
                                <label for="q08" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q08">
                                    <p>камеры</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q09" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q09">
                                    <p>система  Safity Drive</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q10" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q10">
                                    <p>расположение VIN кода</p>
                                </label>
                                <label for="q101" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="" id="q101">
                                    <p>горизонтально</p>
                                </label>
                                <label for="q102" class="col-xs-12 col-sm-6">
                                    <input type="radio" name="" id="q102">
                                    <p>вертикально</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q11" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q11">
                                    <p>встроенные антенны</p>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="row">
                                <label for="q12" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q12">
                                    <p>молдинг</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q13" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q13">
                                    <p>кассета</p>
                                </label>
                            </div>
                            <div class="row">
                                <label for="q14" class="col-xs-12 col-sm-12">
                                    <input type="checkbox" name="" id="q14">
                                    <p>для праворульных машин</p>
                                </label>
                            </div>
                            <div class="row"><input type="reset" value="Сбросить фильтр"></div>
                        </div>
                    </div>
                    <h4>Выберите, какая ценовая категория подойдет вам больше</h4>
                    <div class="radioblock clearfix">
                        <div class="col-md-3 item">
                            <label for="t1" value="ekonom">

                                <input type="radio" name="tarif" id="t1"  ">

                                <p class="title">экономный вариант</p>
                                <p class="price">Цены от 5600 Р</p>
                                <div class="text">
                                    <p>Это всемирно известные бренды с многолетней историей и хорошей репутацией проверенных поставщиков на автоконвейеры. К этой ценовой категории относят стекла AGC, Pilkington и Securit. Продукция Pilkington и Securit полностью импортная, а AGC частично производятся на Борском Стекольном Заводе в Нижегородсткой области.</p>
                                    <a href=""><span>Подробнее</span></a>
                                </div>
                                <div class="img">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand1.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand2.png" alt="">
                                </div>
                            </label>
                        </div>
                        <div class="col-md-3 item">
                            <label for="t2" >

                                <input type="radio" value="bisnes" name="tarif" id="t2" ">

                                <p class="title">Бизнес стекло</p>
                                <p class="price">Цены от 15600 Р</p>
                                <div class="text">
                                    <p>Это всемирно известные бренды с многолетней историей и хорошей репутацией проверенных поставщиков на автоконвейеры. К этой ценовой категории относят стекла AGC, Pilkington и Securit. Продукция Pilkington и Securit полностью импортная, а AGC частично производятся на Борском Стекольном Заводе в Нижегородсткой области.</p>
                                    <a href=""><span>Подробнее</span></a>
                                </div>
                                <div class="img">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand1.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand2.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand1.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand2.png" alt="">
                                </div>
                            </label>
                        </div>
                        <div class="col-md-3 item">
                            <label for="t3" >

                                <input type="radio" value="premium" name="tarif" id="t3"">

                                <p class="title">Премиум стекло</p>
                                <p class="price">Цены от 22600 Р</p>
                                <div class="text">
                                    <p>Это всемирно известные бренды с многолетней историей и хорошей репутацией проверенных поставщиков на автоконвейеры. К этой ценовой категории относят стекла AGC, Pilkington и Securit. Продукция Pilkington и Securit полностью импортная, а AGC частично производятся на Борском Стекольном Заводе в Нижегородсткой области.</p>
                                    <a href=""><span>Подробнее</span></a>
                                </div>
                                <div class="img">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand1.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand2.png" alt="">
                                    <img src="<?php bloginfo('template_directory')?>/img/brand1.png" alt="">
                                </div>
                            </label>
                        </div>
                        <div class="col-md-3 item">
                            <label for="t4" >
                                <input type="radio" value="budget" name="tarif" id="t4" ">
                                <p class="title">Бюджетное стекло</p>

                                <p class="price">Цены от 10600 Р</p>
                                <div class="text">
                                    <p>Это всемирно известные бренды с многолетней историей и хорошей репутацией проверенных поставщиков на автоконвейеры. К этой ценовой категории относят стекла AGC, Pilkington и Securit. Продукция Pilkington и Securit полностью импортная, а AGC частично производятся на Борском Стекольном Заводе в Нижегородсткой области.</p>
                                    <a href=""><span>Подробнее</span></a>
                                </div>
                            </label>
                        </div>
                    </div>

                    </div>

                    <div class="res_search">
                        <h3>Мы нашли для вас 0 стекол(ла)</h3>

                        <div class="sorter_dock">
                            <p>Сортировать по:</p>
                            <a href="#" >Популярности</a>
                            <a href="#" class="sort" data-param="comparesum">Цене</a>
                            <a href="#">Рейтингу</a>
                            <a href="#" class="sort" data-param="comparemanufacturer">Производителю</a>
                        </div>
                        <div class="result">

                        </div>
                    </div>

                    <div class="butt">
                        <p class="title">Выбрали? Тогда мы можем перейти к следующему шагу</p>
                        <button class="btn-primary">Подобрать уц <i></i></button>
                        <p>или</p>
                        <button class="btn-default">выбрать еще стекло</button>
                    </div>
                    <span>Возникли вопросы?<br/>Звоните: +7 (499) 686-08-45</span>
                </form>
            </div>
        </div>
    </div>
</main>
<!--<div class="stick hidden-xs">-->
<!--    <div class="item">-->
<!--        <div class="title">-->
<!--            <p>Вы выбрали</p>-->
<!--            <a href="#"></a>-->
<!--        </div>-->
<!--        <div class="active">-->
<!--            <p>Авто</p>-->
<!--            <strong>AUDI A6 V8 SED 4D 1999-2004</strong>-->
<!--            <a href="#" class="edit" title="Редактировать"></a>-->
<!--        </div>-->
<!--        <div class="glass">-->
<!--            <p>Параметры стекла</p>-->
<!--            <div class="items-item">-->
<!--                <strong>Лобовое стекло</strong>-->
<!--                <p>зеленое с голубой полосой</p>-->
<!--                <ul>-->
<!--                    <li>-->
<!--                        <img src="--><?php //bloginfo('template_directory')?><!--/img/st_gl.png" alt="">-->
<!--                        <p>Цена стекла:<b>4700 Р</b></p>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <a href="#" class="edit"></a>-->
<!--            </div>-->
<!--            <div class="items-item">-->
<!--                <strong>боковое стекло</strong>-->
<!--                <ul>-->
<!--                    <li>-->
<!--                        <img src="--><?php //bloginfo('template_directory')?><!--/img/st_gl.png" alt="">-->
<!--                        <p>Цена стекла:<b>4700 Р</b><br /><span>5400 Р</span></p>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <a href="#" class="edit"></a>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="center">-->
<!--            <p>Установочный центр</p>-->
<!--            <div class="items-item">-->
<!--                <strong>Ярославское шоссе, ул. 9-ый Ленинский пер-к д.11 (Медведково)</strong>-->
<!--                <span>Стрим Авто Мытищи</span>-->
<!--                <p>Срок доставки:<br/>2 дня</p>-->
<!--                <a href="#" class="edit" title="Редактировать"></a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="item">-->
<!--        <div class="title">-->
<!--            <p>Вы выбрали</p>-->
<!--            <a href="#"></a>-->
<!--        </div>-->
<!--        <div class="active">-->
<!--            <p>Авто</p>-->
<!--            <strong>AUDI A6 V8 SED 4D 1999-2004</strong>-->
<!--            <a href="#" class="edit" title="Редактировать"></a>-->
<!--        </div>-->
<!--        <div class="glass">-->
<!--            <p>Параметры стекла</p>-->
<!--            <div class="items-item">-->
<!--                <strong>Лобовое стекло</strong>-->
<!--                <p>зеленое с голубой полосой</p>-->
<!--                <ul>-->
<!--                    <li>-->
<!--                        <img src="--><?php //bloginfo('template_directory')?><!--/img/st_gl.png" alt="">-->
<!--                        <p>Цена стекла:<b>4700 Р</b></p>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <a href="#" class="edit"></a>-->
<!--            </div>-->
<!--            <div class="items-item">-->
<!--                <strong>боковое стекло</strong>-->
<!--                <ul>-->
<!--                    <li>-->
<!--                        <img src="--><?php //bloginfo('template_directory')?><!--/img/st_gl.png" alt="">-->
<!--                        <p>Цена стекла:<b>4700 Р</b><br /><span>5400 Р</span></p>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <a href="#" class="edit"></a>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="center">-->
<!--            <p>Установочный центр</p>-->
<!--            <div class="items-item">-->
<!--                <strong>Ярославское шоссе, ул. 9-ый Ленинский пер-к д.11 (Медведково)</strong>-->
<!--                <span>Стрим Авто Мытищи</span>-->
<!--                <p>Срок доставки:<br/>2 дня</p>-->
<!--                <a href="#" class="edit" title="Редактировать"></a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<?php
get_footer();
?>