<?php
/**
 * Template Name: step-three
 *
 */
get_header();
?>

    <main>
        <div class="container">
            <div class="row breadcrumb">
                <ul class="col-sm-12">
                    <li><a href="#">Главная</a></li>
                    <li><span>Подбор стекла</span></li>
                </ul>
                <h1 class="col-sm-12">ПОДБОР СТЕКЛА В АВТОСТЕКЛОФФ</h1>
            </div>
            <div class="row title">
                <div class="graphic step3">
                    <div class="col-sm-4"><a href="#"></a></div>
                    <div class="col-sm-4"><a href="#"></a></div>
                    <div class="col-sm-4"><a href="#"></a></div>
                </div>
            </div>
        </div>
        <div class="container">
            <h3 class="lets">давайте вместе выберем установочный центр</h3>
            <div class="view_nav button_page_three">
                <ul>
                    <li class="active">
                        <a href="#">
                            <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_list.png" alt="">
                            <span>Список</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_map.png" alt="">
                            <span>Карта</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="gmap">
            <div class="view_nav left_view_nav">
                <ul>
                    <li>
                        <a href="">
                            <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_list.png" alt="">
                            <span>Все</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="">
                            <img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_map.png" alt="">
                            <span>Избранное</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="map_place">

            </div>

            <div class="legend">
                <ul>
                    <li>
                        <img src="<?php bloginfo('template_directory'); ?>/img/marker.png" alt="">
                        <p>Установочные центры</p>
                    </li>
                    <li>
                        <img src="<?php bloginfo('template_directory'); ?>/img/marker-s.png" alt="">
                        <p>Избранное</p>
                    </li>
                </ul>
            </div>

        </div>
        <div class="bg">
            <div class="container">
                <div class="row content choose_center">

                    <form>
                        <div class="sorter_dock">
                            <p>Сортировать по:</p>
                            <a href="#">Популярности</a>
                            <a href="#">Цене</a>
                            <a href="#">Рейтингу</a>
                            <a href="#">Производителю</a>
                        </div>

                        <div class="result_centers">
                            <div class="item open">
                                <div class="clickable">
                                    <div class="col-md-1 col-sm-1 col-xs-2">
                                        <a href="#" class="show"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-5 col-xs-10 title">
                                        <strong>Мытищи<br/>Ярославское шоссе, ул. 9-ый Ленинский переулок д.11 (Медведково)</strong>
                                        <p>Работаем c 08:00 до 22:00</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Срок доставки:</span>
                                        <p>2 дня</p>
                                        <span>Предоплата:</span>
                                        <p>нет</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Итоговая цена:</span>
                                        <strong>5700 Р</strong>
                                        <button class="btn-primary">Записаться</button>
                                        <a href="#">Избранное</a>
                                    </div>
                                </div>
                                <div class="col-sm-12 body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                                            <div class="title">
                                                <img src="<?php bloginfo('template_directory'); ?>/img/cntr1.png" alt="">
                                                <p><b>Стрим Авто Мытищи</b>Официальный</p>
                                                <p>Рейтинг <strong>4.50</strong>
                                                    <br><span>325 оценок</span>
                                                </p>
                                                <p>Цена установки:<b>300 Р</b></p>
                                                <p>Цена доставки:<b>700 Р</b></p>
                                            </div>
                                            <div>
                                                <p>Координаты для навигатора:</p>
                                                <p><b>GPS: 55.7824 , 37.5099.</b></p>
                                                <p>В сторону центра по Хорошевскому ш. до метро Полежаевская, на светофоре разворот в противоположную сторону, через 300м направо, на ул.Зорге, через 800м уц</p>
                                            </div>
                                            <div class="pay">
                                                <div class="left">
                                                    <p>Высота ворот:</p>
                                                    <p><b>3 м</b></p>
                                                </div>
                                                <div class="right">
                                                    <p>Способ оплаты:</p>
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay1.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay2.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay3.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay4.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay5.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay6.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay7.png" alt="">
                                                </div>
                                            </div>
                                            <div class="ext">
                                                <p>Дополнительные услуги:</p>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">WI-FI</strong>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">Комната отдыха</strong>
                                            </div>
                                            <div class="review">
                                                <h4>Отзывы</h4>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                        <p>Купил сразу два телека - этот и Рубин 43" - пульты ДУ - точная копия. Только надписями отличаются. Оба телека управляются с одного пульта.</p>
                                                        <p>Общие впечатления "Рубин", кстати, внешне идентичен, только ножки другие. И звук тоже как у Шиваки. Всякие MP4 не играет - только MKV. MKV смотрел - отлично. Все плавно, не тормозит. Картинка отличная. От функций SMART TV отказался - то что встраивают в недорогие телевизоры - все равно использовать практически невозможно. Специально взял самый недорогой ТВ с цифровыми входами и приличной для кухни диагональю. Заказал из Китая ТВ-приставку на андроиде - там и любые форматы аудио/видео, и встроенный Wi-Fi с интернетом и цена 2 тыс. руб, и с собой на дачу можно брать, а интернет c телефона раздавать. Если бы не звук, то поставил бы 5 звезд - для кухни сойдет, конечно. Не подключать же ресивер с аудиосистемой - его и ставить негде.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="photo hidden-xs">
                                                <strong>Фото</strong>
                                                <a href="#" class="fancy">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt="">
                                                </a>
                                                <ul>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div class="videos hidden-xs">
                                                <strong>Видео</strong>
                                                <video src="" poster="<?php bloginfo('template_directory'); ?>/img/vid_th.png"></video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="clickable">
                                    <div class="col-md-1 col-sm-1 col-xs-2">
                                        <a href="#" class="show"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-5 col-xs-10 title">
                                        <strong>Мытищи<br/>Ярославское шоссе, ул. 9-ый Ленинский переулок д.11 (Медведково)</strong>
                                        <p>Работаем c 08:00 до 22:00</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Срок доставки:</span>
                                        <p>2 дня</p>
                                        <span>Предоплата:</span>
                                        <p>нет</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Итоговая цена:</span>
                                        <strong>5700 Р</strong>
                                        <button class="btn-primary">Записаться</button>
                                        <a href="#">Избранное</a>
                                    </div>
                                </div>
                                <div class="col-sm-12 body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                                            <div class="title">
                                                <img src="<?php bloginfo('template_directory'); ?>/img/cntr1.png" alt="">
                                                <p><b>Стрим Авто Мытищи</b>Официальный</p>
                                                <p>Рейтинг <strong>4.50</strong>
                                                    <br><span>325 оценок</span>
                                                </p>
                                                <p>Цена установки:<b>300 Р</b></p>
                                                <p>Цена доставки:<b>700 Р</b></p>
                                            </div>
                                            <div>
                                                <p>Координаты для навигатора:</p>
                                                <p><b>GPS: 55.7824 , 37.5099.</b></p>
                                                <p>В сторону центра по Хорошевскому ш. до метро Полежаевская, на светофоре разворот в противоположную сторону, через 300м направо, на ул.Зорге, через 800м уц</p>
                                            </div>
                                            <div class="pay">
                                                <div class="left">
                                                    <p>Высота ворот:</p>
                                                    <p><b>3 м</b></p>
                                                </div>
                                                <div class="right">
                                                    <p>Способ оплаты:</p>
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay1.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay2.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay3.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay4.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay5.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay6.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay7.png" alt="">
                                                </div>
                                            </div>
                                            <div class="ext">
                                                <p>Дополнительные услуги:</p>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">WI-FI</strong>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">Комната отдыха</strong>
                                            </div>
                                            <div class="review">
                                                <h4>Отзывы</h4>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                        <p>Купил сразу два телека - этот и Рубин 43" - пульты ДУ - точная копия. Только надписями отличаются. Оба телека управляются с одного пульта.</p>
                                                        <p>Общие впечатления "Рубин", кстати, внешне идентичен, только ножки другие. И звук тоже как у Шиваки. Всякие MP4 не играет - только MKV. MKV смотрел - отлично. Все плавно, не тормозит. Картинка отличная. От функций SMART TV отказался - то что встраивают в недорогие телевизоры - все равно использовать практически невозможно. Специально взял самый недорогой ТВ с цифровыми входами и приличной для кухни диагональю. Заказал из Китая ТВ-приставку на андроиде - там и любые форматы аудио/видео, и встроенный Wi-Fi с интернетом и цена 2 тыс. руб, и с собой на дачу можно брать, а интернет c телефона раздавать. Если бы не звук, то поставил бы 5 звезд - для кухни сойдет, конечно. Не подключать же ресивер с аудиосистемой - его и ставить негде.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="photo hidden-xs">
                                                <strong>Фото</strong>
                                                <a href="#" class="fancy">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt="">
                                                </a>
                                                <ul>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div class="videos hidden-xs">
                                                <strong>Видео</strong>
                                                <video src="" poster="<?php bloginfo('template_directory'); ?>/img/vid_th.png"></video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="clickable">
                                    <div class="col-md-1 col-sm-1 col-xs-2">
                                        <a href="#" class="show"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-5 col-xs-10 title">
                                        <strong>Мытищи<br/>Ярославское шоссе, ул. 9-ый Ленинский переулок д.11 (Медведково)</strong>
                                        <p>Работаем c 08:00 до 22:00</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Срок доставки:</span>
                                        <p>2 дня</p>
                                        <span>Предоплата:</span>
                                        <p>нет</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Итоговая цена:</span>
                                        <strong>5700 Р</strong>
                                        <button class="btn-primary">Записаться</button>
                                        <a href="#">Избранное</a>
                                    </div>
                                </div>
                                <div class="col-sm-12 body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                                            <div class="title">
                                                <img src="<?php bloginfo('template_directory'); ?>/img/cntr1.png" alt="">
                                                <p><b>Стрим Авто Мытищи</b>Официальный</p>
                                                <p>Рейтинг <strong>4.50</strong>
                                                    <br><span>325 оценок</span>
                                                </p>
                                                <p>Цена установки:<b>300 Р</b></p>
                                                <p>Цена доставки:<b>700 Р</b></p>
                                            </div>
                                            <div>
                                                <p>Координаты для навигатора:</p>
                                                <p><b>GPS: 55.7824 , 37.5099.</b></p>
                                                <p>В сторону центра по Хорошевскому ш. до метро Полежаевская, на светофоре разворот в противоположную сторону, через 300м направо, на ул.Зорге, через 800м уц</p>
                                            </div>
                                            <div class="pay">
                                                <div class="left">
                                                    <p>Высота ворот:</p>
                                                    <p><b>3 м</b></p>
                                                </div>
                                                <div class="right">
                                                    <p>Способ оплаты:</p>
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay1.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay2.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay3.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay4.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay5.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay6.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay7.png" alt="">
                                                </div>
                                            </div>
                                            <div class="ext">
                                                <p>Дополнительные услуги:</p>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">WI-FI</strong>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">Комната отдыха</strong>
                                            </div>
                                            <div class="review">
                                                <h4>Отзывы</h4>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                        <p>Купил сразу два телека - этот и Рубин 43" - пульты ДУ - точная копия. Только надписями отличаются. Оба телека управляются с одного пульта.</p>
                                                        <p>Общие впечатления "Рубин", кстати, внешне идентичен, только ножки другие. И звук тоже как у Шиваки. Всякие MP4 не играет - только MKV. MKV смотрел - отлично. Все плавно, не тормозит. Картинка отличная. От функций SMART TV отказался - то что встраивают в недорогие телевизоры - все равно использовать практически невозможно. Специально взял самый недорогой ТВ с цифровыми входами и приличной для кухни диагональю. Заказал из Китая ТВ-приставку на андроиде - там и любые форматы аудио/видео, и встроенный Wi-Fi с интернетом и цена 2 тыс. руб, и с собой на дачу можно брать, а интернет c телефона раздавать. Если бы не звук, то поставил бы 5 звезд - для кухни сойдет, конечно. Не подключать же ресивер с аудиосистемой - его и ставить негде.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="photo hidden-xs">
                                                <strong>Фото</strong>
                                                <a href="#" class="fancy">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt="">
                                                </a>
                                                <ul>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div class="videos hidden-xs">
                                                <strong>Видео</strong>
                                                <video src="" poster="<?php bloginfo('template_directory'); ?>/img/vid_th.png"></video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="clickable">
                                    <div class="col-md-1 col-sm-1 col-xs-2">
                                        <a href="#" class="show"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-5 col-xs-10 title">
                                        <strong>Мытищи<br/>Ярославское шоссе, ул. 9-ый Ленинский переулок д.11 (Медведково)</strong>
                                        <p>Работаем c 08:00 до 22:00</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Срок доставки:</span>
                                        <p>2 дня</p>
                                        <span>Предоплата:</span>
                                        <p>нет</p>
                                    </div>
                                    <div class="col-md-2 col-sm-3 col-xs-6">
                                        <span>Итоговая цена:</span>
                                        <strong>5700 Р</strong>
                                        <button class="btn-primary">Записаться</button>
                                        <a href="#">Избранное</a>
                                    </div>
                                </div>
                                <div class="col-sm-12 body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                                            <div class="title">
                                                <img src="<?php bloginfo('template_directory'); ?>/img/cntr1.png" alt="">
                                                <p><b>Стрим Авто Мытищи</b>Официальный</p>
                                                <p>Рейтинг <strong>4.50</strong>
                                                    <br><span>325 оценок</span>
                                                </p>
                                                <p>Цена установки:<b>300 Р</b></p>
                                                <p>Цена доставки:<b>700 Р</b></p>
                                            </div>
                                            <div>
                                                <p>Координаты для навигатора:</p>
                                                <p><b>GPS: 55.7824 , 37.5099.</b></p>
                                                <p>В сторону центра по Хорошевскому ш. до метро Полежаевская, на светофоре разворот в противоположную сторону, через 300м направо, на ул.Зорге, через 800м уц</p>
                                            </div>
                                            <div class="pay">
                                                <div class="left">
                                                    <p>Высота ворот:</p>
                                                    <p><b>3 м</b></p>
                                                </div>
                                                <div class="right">
                                                    <p>Способ оплаты:</p>
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay1.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay2.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay3.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay4.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay5.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay6.png" alt="">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/pay7.png" alt="">
                                                </div>
                                            </div>
                                            <div class="ext">
                                                <p>Дополнительные услуги:</p>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">WI-FI</strong>
                                                <strong><img src="<?php bloginfo('template_directory'); ?>/img/ic/ic_check.png" alt="">Комната отдыха</strong>
                                            </div>
                                            <div class="review">
                                                <h4>Отзывы</h4>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="title">
                                                        <strong>Владислав</strong>
                                                        <p>20 декабря 2015</p>
                                                    </div>
                                                    <div class="rating">
                                                        <p>Оценка</p>
                                                        <strong>4.50</strong>
                                                    </div>
                                                    <div class="text">
                                                        <p>Недостатки С аналогового входа картинка троится, квадратится сильно. На этой же антенне TFT Toshiba с форматом 4:3 почти идеально показывал. Но это если близко сидеть. С расстояния больше метра я уже не замечаю. Вот звук - пичалька. У всех самсунгов заметно лучше. При включении объемного звучания - вообще как из унитаза - лучше не трогать.</p>
                                                        <p>Купил сразу два телека - этот и Рубин 43" - пульты ДУ - точная копия. Только надписями отличаются. Оба телека управляются с одного пульта.</p>
                                                        <p>Общие впечатления "Рубин", кстати, внешне идентичен, только ножки другие. И звук тоже как у Шиваки. Всякие MP4 не играет - только MKV. MKV смотрел - отлично. Все плавно, не тормозит. Картинка отличная. От функций SMART TV отказался - то что встраивают в недорогие телевизоры - все равно использовать практически невозможно. Специально взял самый недорогой ТВ с цифровыми входами и приличной для кухни диагональю. Заказал из Китая ТВ-приставку на андроиде - там и любые форматы аудио/видео, и встроенный Wi-Fi с интернетом и цена 2 тыс. руб, и с собой на дачу можно брать, а интернет c телефона раздавать. Если бы не звук, то поставил бы 5 звезд - для кухни сойдет, конечно. Не подключать же ресивер с аудиосистемой - его и ставить негде.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="photo hidden-xs">
                                                <strong>Фото</strong>
                                                <a href="#" class="fancy">
                                                    <img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt="">
                                                </a>
                                                <ul>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                    <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/img/rev_th.png" alt=""></a></li>
                                                </ul>
                                            </div>
                                            <div class="videos hidden-xs">
                                                <strong>Видео</strong>
                                                <video src="" poster="<?php bloginfo('template_directory'); ?>/img/vid_th.png"></video>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="butt">
                            <p class="title">Выбрали? Тогда мы можем перейти к следующему шагу</p>
                            <button class="btn-default">выбрать еще стекло</button>
                            <p>или</p>
                            <button class="btn-primary">Оформить заказ <i></i></button>
                        </div>
                        <span>Возникли вопросы?<br/>Звоните: +7 (499) 686-08-45</span>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php get_footer(); ?>